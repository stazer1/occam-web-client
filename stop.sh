#!/bin/bash

_OCCAM_WEB_PATH=$(realpath $(dirname $0))

# We want to place ourselves in our vendor environment, if it exists
if [ -e "${_OCCAM_WEB_PATH}/vendor/bin" ]; then
	export PATH=${_OCCAM_WEB_PATH}/vendor/bin:$PATH
	export LD_LIBRARY_PATH=${_OCCAM_WEB_PATH}/vendor/lib:$LD_LIBRARY_PATH
fi

vendor/bin/pumactl stop -F config/puma.rb $@
