source 'https://rubygems.org'
ruby '>=2.6','>=3.0'

# We need this for puma, sometimes...
gem 'irb'

gem 'ffi'
gem 'rss'

# Web Framework
gem 'sinatra', '~> 2.1.0'
gem 'sinatra-contrib', '~> 2.1.0'

# Asset Management
gem 'sprockets', '~> 4.0'

# Ruby Documentation
gem 'yard'

# API Documentation
gem 'yard-sinatra', :git => "https://github.com/rkh/yard-sinatra"

# Markup Rendering Engine
gem 'slim', '~> 4.0.0'     # Slim
gem 'redcarpet', :git => "https://github.com/vmg/redcarpet" # Markdown

# Time duration markup (270s => "4 mins 30 secs")
gem 'chronic_duration'

# Internationalization
gem 'i18n'         # Main localization library
gem 'rails-i18n',  # Rails oriented default localizations
  :require => nil  #  (gives us default time/date localization)

# Syntax highlight for documentation generation
gem 'rouge'

# URL-Safe String Processing
gem 'stringex'

# For color handling
gem 'chroma'

# Runs Rakefiles
gem 'rake'

# For handling revision and object hashes
# (tests) For creating random object ids and identities
gem 'multihashes'
gem 'base58'

# Testing environment libraries
group :test, :optional => true do
  gem 'capybara', '~> 3.37.0', :require => 'capybara/dsl'
  gem 'fabrication', '~> 1.2.0'
  gem 'rack-test', '~> 0.8.3', :require => 'rack/test'
  gem 'minitest', '~> 5.11.3', :require => 'minitest/autorun'
  gem "ansi"               # minitest colors
  gem "minitest-reporters" # minitest output
  gem "mocha", "~> 1.1.0"  # stubs
  gem 'simplecov', require: false # Code Coverage
  gem 'nokogiri', '~> 1.13.0' # Validates HTML generation
  gem 'poltergeist'
  gem 'selenium-webdriver'
  gem 'chromedriver-helper'
  gem 'capybara-selenium'
  gem 'pry-byebug'

  # Javascript testing
  gem "jasmine"
end

# Web Server
gem 'puma'

# WebSocket support
gem "faye-websocket"

# Sass (Stylesheet Format)
gem 'sass'

# Javascript Compression
gem 'uglifier'

# Cookie Encryptor
gem 'encrypted_cookie'

# Random Default Avatars
gem 'ruby_identicon'
group :avatarly, optional: true do
  gem 'avatarly'
end

# QR Code Generator
gem 'rqrcode'

# Error logging and tracking
group :airbrake, optional: true do
  gem 'airbrake', '~> 5.0'
end

# Connection Pool
gem 'connection_pool'
