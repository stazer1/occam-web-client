# Occam Web

Occam is a digital preservation ecosystem that focuses on interactive, computation artifacts.
This supplies a web-server and direct support for running HTML and JavaScript interactive content.
For the code that serves as the daemon and object store, see [occam core](https://gitlab.com/occam-archive/occam).

## Pre-Install Setup

Start an Occam daemon. (See the instructions at the [occam daemon](https://gitlab.com/occam-archive/occam).)

Install ruby >=2.4.1 through your system package manager or through a tool such as `rvm`.
Install bundler with `gem install bundler` if you don't already have it.
Install git.

Clone this repository:

```
git clone https://gitlab.com/occam-archive/occam-web-client
```

Change into the created directory:

```
cd occam-web-client
```

### Installing Automatically

If you are using a system such as Ubuntu, you can use a single script to install the rest of the system:

```
./scripts/install.sh
```

This script will install every dependency and build all associated content and assets when necessary.

### Installing Manually

For the sake of incompatible systems or more advanced installation requirements, you may follow this section which exhaustively lists each command required to install the system.

Install libraries:

```
bundle install
```

Optionally, install libraries to a vendored path:

```
bundle install --path vendor/bundle
```

Install node.js through your system package manager.
Using npm (node's package manager), install the JavaScript dependencies.

```
npm install
```

Now, we can compile the JavaScript using [webpack](https://webpack.js.org/) and [babel](https://babeljs.io/), which
lets us use future JavaScript features and cross-compile them for older browsers.
The following command will download vendored dependencies and also produce the required packaged JavaScript.
This produces the file "./public/js/occam.js" and fills the "./public/js/vendor" directory.

```
npm run build
```

Optionally, to build the documentation so that it can be rendered in the web portal:

```
npm run build-docs
npm run build-css-docs
```

Now we can run the server:

```
bundle exec puma
```

Go to [http://localhost:9292](https://localhost:9292)

## Testing

If you are working with the code directly, you may want to periodically run the test suite.
To run the tests, you need to ensure the testing libraries are installed:

```
bundle install --with test
```

Again, if you want to optionally install the dependencies in a local path instead of a system path:

```
bundle install --with test --path vendor/bundle
```

And then you invoke the test suite by running:

```
rake test
```

For more information, see [the spec directory](spec/README.md).

### JavaScript Testing

For testing the client facing JavaScript code, we will be using [karma](https://karma-runner.github.io/) alongside
[Jasmine](https://jasmine.github.io/). The tests are located in `spec/js` and are invoked with:

```
npx karma start
```

This will run the test suite using any headless browser that you have currently installed that has also been configured
in our testing configuration in `karma.conf.js`.

For more information, see [the spec/js directory](spec/js/README.md).

## Generating Documentation

### Daemon Documentation

The web-client will automatically serve the Daemon (occam proper) documentation as long as it has already been generated.
Refer to Occam's own documentation for how to generate the HTML documentation.

### Web and API Documentation

We generate our Ruby code documentation and API documentation using [Yard](https://yardoc.org/).

To generate documentation for the website itself, including documentation of the API and routes, do the following.

```
sh build-docs.sh
```

This will invoke yard (via yardoc) and generate documentation in the `public/docs` and `public/apidocs` directories.

### JavaScript Documentation

To generate documentation of the JavaScript code, we can invoke [documentation.js](http://documentation.js.org/):

```
npm run build-docs
```

**Optionally**: To run the equivalent command directly, in order to add any custom options, you would use:

```
npx documentation build public/js/new_app.js -f html -o public/jsdocs
```

And then we can navigate (when the webserver is running) to <http://localhost:9292/jsdocs> to see the documentation viewer.

## Deploying to Production

### Install occam-web Service

```
sudo cp docs/systemd/occam.service /etc/systemd/system/occam-web.service
```

Edit the newly copied file to reflect the occam username and shell on your system.

Enable the occam-web service.

```
sudo systemctl enable occam-web
sudo systemctl start occam-web
```

### Setup Error Logging (Optional)

When doing a production deployment, the easiest way to track errors users encounter is to tell the front-end to error reports to an Errbit server via Airbrake.

```
wget https://github.com/errbit/errbit/archive/v0.9.0.tar.gz
tar -xf v0.9.0.tar.gz
cd errbit-0.9.0/
sudo apt install docker.io docker-compose
sudo usermod -aG docker USERNAME
```

Logout and log back in.

```
cd errbit-0.9.0/
docker-compose up
```

In another terminal, generate the initial login you'll need to configure the app:

```
docker-compose exec errbit bundle exec rake errbit:bootstrap
```

You will get an output like this:

```
Notice: no rspec tasks available in this environment
Overwriting existing field _id in class App.
Seeding database
-------------------------------
Creating an initial admin user:
-- email:    errbit@errbit.example.com
-- password: bVcwT6II_I-U
```

Navigate to the Errbit webpage on the local server, and log in using those credentials. Click "Add a New App". Fill in the form, then copy the project_key for the configuration below.

Configure all production Occam front-ends to send their Airbrake traffic to the Errbit server that was just set up.

In the web configuration on each Occam front-end:

```
# Airbrake settings. If airbrake is being used, errors seen at the ruby level
# will be sent to the server specified in the configuration.
airbrake:
  # Whether or not to use airbrake.
  enabled: true

  # Where airbrake/errbit is running.
  host: 'http://your-errbit-server.example.com:8080'

  # Unused by Errbit, but required by Airbrake. This can be any valid positive
  # integer.
  project-id: 1

  # API key generated by an Airbrake compatible server.
  project-key: 'ba94550ce5f3cfefdb091b48adb67778'
```

Install the Airbrake group of gems.

```
<OCCAM_WEB_DIR>/vendor/bin/bundle install --with airbrake
```

Now when the front-end is run errors will be logged to Errbit.

Restart the front-end if it is running.

```
sudo systemctl restart occam-web
```

## Project Layout

```
Gemfile         - List of package dependencies
Gemfile.lock    - The frozen version information for those dependencies
LICENSE.txt     - Copyright information (AGPL 3.0)
Rakefile        - Scripts that manage certain aspects of the site; runs tests
CODE_OF_CONDUCT - Code of conduct for community interaction.
/lib            - Contains the main application and support code.
/helpers        - General code that is used throughout the site
/spec           - Testing code
  | js          - JavaScript Tests
/models         - Classes that wrap data structures used in Occam.
  | account     - Describes an authorized person (one who can log in)
  | authorship  - Describes an attached person for an object
  | job         - Describes a particular deployed task
  | layout      - Describes the parameters of the rendered page
  | link        - Describes a relationship between two objects
  | object      - Describes an object!
  | person      - Describes an actor in the system
  | review_c... - Describes a review token to allow anonymous viewing
  | system      - Describes the system at large
  | task        - Describes a task that runs or views an object
  | workflow    - Describes a set of interconnected objects
/controllers    - Web routes
  | accounts    - Routes related to authorizable people
  | objects     - Routes related to object retrieval
  | people      - Routes related to all people
  | search      - Routes related to general search
  | static      - Routes related to static pages (404, 500, etc)
  | system      - Routes related to system stats and health
  | task        - Routes related to task generation
/views          - HTML templates for rendering the content
  | stylesheets - Sass files for the css stylesheets
/locales        - Internationalization
/config         - Library configuration
/public         - Static content, such as JavaScript and images, served directly
  | js          - Front-end Scripts
  | images      - Static images
  | css         - Static css
  | fonts       - Downloaded fonts
/key            - [Generated] contains the self-signed keys
```

## Contributing

The following are accepted and cherished forms of contribution:

* Filing a bug issue. (We like to see feature requests and just normal bugs)
* Fixing a bug. (It's obviously helpful!)
* Adding documentation. (Help us with our docs or send us a link to your blog post!)
* Adding features.
* Adding artwork. (Art is the true visual form of professionalism)

The following are a bit harder to really accept, in spite of the obvious effort that may go into them, so please avoid this:

* Changing all of the JavaScript to coffeescript because it is "better"
* Rewriting all of the sass to whatever is newer. (It's happened to me before)
* Porting everything to rails.
* Creating a pull request with a "better" software license.

In general, contributions are easily provided by doing one of the following:

* Fork and clone the project.
* Update the code on your end however you see fit.
* Push that code to a public server.
* Create a pull request from your copy to ours.

The above is the most convenient process. You may create an issue with a link to a repository or tar/zip containing your code or patches as well, if git is not your thing.

## Acknowledgements

All attribution and crediting for contributors is located within [this file](views/static/credits.md). Or by visiting `/acknowledgements` in any Occam website.

## Code of Conduct

Please note that this project is released with a [Contributor Code of Conduct](CODE_OF_CONDUCT.md). By participating in this project you agree to abide by its terms.

## License

Occam is licensed under the AGPL 3.0. Refer to the [LICENSE.txt](LICENSE.txt) file in the root of the repository for specific details.
