# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Controller
    module Helpers
      # This module implements the search interpretation.
      #
      # Search has a lot of facets and form elements that must be interpreted
      # and passed to the Object.fullSearch function. This helps do that and
      # provides a set of data representing the search results at the end.
      module SearchHelpers
        def performSearch(options={})
          query = params["search"] || params["query"]

          # Pass along the off types as excluded types
          excludeTypes = options[:excludeTypes] || []
          params["search-facet-type-name"] = (params["search-facet-type-name"] || {}).values
          params["search-facet-type"] = (params["search-facet-type"] || {}).values
          (params["search-facet-type-name"] || []).zip(params["search-facet-type"] || []).each do |type, value|
            if value != "on"
              if !excludeTypes.include?(type)
                excludeTypes << type
              end
            elsif excludeTypes.include?(type)
              excludeTypes.delete(type)
            end

            if value == "on" && !params["type"]
              params["type"] = type
            end
          end

          data    = {}
          objects = []
          types   = []
          tags    = []
          excludedTags = []

          tags = []

          # Pre-populate tags
          if params["tags"]
            tags = params["tags"].split(';')
          end

          params["search-facet-tag-name"] = (params["search-facet-tag-name"] || {}).values
          params["search-facet-tag"] = (params["search-facet-tag"] || {}).values
          (params["search-facet-tag-name"] || []).zip(params["search-facet-tag"] || []).each do |tag, value|
            if value == "on"
              tags << tag
            else
              excludedTags << tag
            end
          end

          if params["types"] == "on"
            types = Object.types(:query => query).map do |type|
              { :type => type }
            end
          end

          environments = []
          knownEnvironments = {}
          params["search-facet-environment-name"] = (params["search-facet-environment-name"] || {}).values
          params["search-facet-environment"] = (params["search-facet-environment"] || {}).values
          (params["search-facet-environment-name"] || []).zip(params["search-facet-environment"] || []).each do |name, value|
            if value == "on"
              environments << name
            end
            knownEnvironments[name] = (value == "on")
          end

          architectures = []
          knownArchitectures = {}
          params["search-facet-architecture-name"] = (params["search-facet-architecture-name"] || {}).values
          params["search-facet-architecture"] = (params["search-facet-architecture"] || {}).values
          (params["search-facet-architecture-name"] || []).zip(params["search-facet-architecture"] || []).each do |name, value|
            if value == "on"
              architectures << name
            end
            knownArchitectures[name] = (value == "on")
          end

          if params["type"] == ""
            params["type"] = nil
          end

          if params["objects"] == "on" || params["types"].nil?
            data = Object.fullSearch(:name          => query,
                                     :type          => params["type"],
                                     :subtype       => params["subtype"],
                                     :environments  => environments,
                                     :architectures => architectures,
                                     :viewsType     => params["viewsType"],
                                     :viewsSubtype  => params["viewsSubtype"],
                                     :identity      => params["identity"],
                                     :tags          => tags,
                                     :excludeTypes  => excludeTypes,
                                     :account       => current_account)
            objects = data[:objects]

            # Keep track of which types are turned off because they obviously won't be
            # in the types list. We have to render them.
            data[:excludeTypes] = excludeTypes

            # Ditto for environments/architectures
            if environments.any?
              data[:knownEnvironments] = knownEnvironments
            end

            if architectures.any?
              data[:knownArchitectures] = knownArchitectures
            end

            data[:includedTags] = tags
            data[:excludedTags] = excludedTags
          end

          {
            :query => query,
            :objects => objects,
            :data => data,
            :types => types
          }
        end
      end
    end

    helpers Helpers::SearchHelpers
  end
end
