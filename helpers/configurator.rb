# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require 'base64'

class Occam
  class Controller
    module Helpers
      module ConfiguratorHelpers
        # Renders a configuration form.
        def renderConfiguration(schema, data = nil, options = {})
          options[:editing] = options[:editing] || false
          options[:prefix] = options[:prefix] || "data"

          key = nil
          nesting = ""
          id = options[:prefix]

          <<~CONFIG
            #{nesting}<occam-configuration>
            #{nesting}  <input type='hidden' name='base64' value='on'></input>
            #{nesting}  <template class='configuration-item' data-i18n-range-error='#{I18n.t('configurations.validations.rangeError')}'><div>#{self.renderConfigurationField({:type => "string", :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-group'><div>#{self.renderConfigurationSchema({}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-boolean'><div>#{self.renderConfigurationField({:type => "boolean", :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-enum'><div>#{self.renderConfigurationField({:type => [], :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-int'><div>#{self.renderConfigurationField({:type => 'int', :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-date'><div>#{self.renderConfigurationField({:type => 'date', :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-datetime'><div>#{self.renderConfigurationField({:type => 'datetime', :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-time'><div>#{self.renderConfigurationField({:type => 'time', :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-number'><div>#{self.renderConfigurationField({:type => 'number', :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-string'><div>#{self.renderConfigurationField({:type => 'string', :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='configuration-item-color'><div>#{self.renderConfigurationField({:type => 'color', :default => "#000000", :label => "Item"}, nil, "item", nesting, id, options[:editing])}</div></template>
            #{nesting}  <template class='validation-item'><li class='configuration-validation'>#{svg(:ui, :test)}#{svg(:ui, :error, :class => "error")}<div class='configuration-validation-message'><p></p></div></li></template>
            #{self.renderConfigurationNavigation(schema, data, key, nesting + "  ", id).chomp}
            #{self.renderConfigurationSchema(schema, data, key, nesting + "  ", id, options[:editing]).chomp}
            #{nesting}</occam-configuration>
          CONFIG
        end

        # Renders the navigation bar for the given configuration schema.
        def renderConfigurationNavigation(schema, data = nil, key = nil, nesting = "", id = nil)
          # TODO: render group and offer a search button in the interface
          <<~CONFIGNAV
            #{nesting}<nav>
            #{nesting}  #{svg(:ui, :search)}
            #{nesting}  <input name='configuration-search' placeholder='#{I18n.t('configurations.filterPlaceholder')}'></input>
            #{nesting}  <button class='configuration-search-clear'>#{svg(:ui, :remove)}</button>
            #{nesting}  <button class='button configuration-collapse-all' title='#{I18n.t('configurations.collapseAll')}'>#{svg(:ui, :collapse)}</button>
            #{nesting}  <div class='dropdown-menu configuration-export'>
            #{nesting}    <button class='button configuration-export dropdown-button' title='#{I18n.t('configurations.export')}'>#{svg(:ui, :download)}</button>
            #{nesting}    <ul class='dropdown-menu-options'>
            #{nesting}      <li><a class='configuration-export-schema-button'>Export Schema</a></li>
            #{nesting}      <li><a class='configuration-export-values-button'>Export Values</a></li>
            #{nesting}    </ul>
            #{nesting}  </div>
            #{nesting}</nav>
          CONFIGNAV
        end

        # Renders a configuration schema of any kind or shape.
        #
        # Presumes that this output will be placed within an
        # `occam-configuration ul` element tree of some kind.
        def renderConfigurationSchema(schema, data = nil, key = nil, nesting = "", id = nil, editing = false)
          if schema.is_a? Hash
            # Determine if this is a configuration field
            if schema.has_key?(:type) && schema[:type] == "array" && schema[:element].is_a?(Hash)
              # A complex array is essentially a group that has multiple items
              self.renderConfigurationArray(schema, data, key, nesting, id, editing)
            elsif schema.has_key?(:type) && !(schema[:type].is_a?(Hash))
              # Render the configuration field (<li> ... </li>)

              self.renderConfigurationField(schema, data, key, nesting, id, editing)
            elsif key.nil?
              # Output base group of configuration fields recursively (<ul> ... </ul>)
              <<~CONFIGGROUP
                #{nesting}<ul class='configuration-group'>
                #{schema.map { |k, v| self.renderConfigurationSchema(v, (data || {})[k], k, nesting + "  ", id, editing).chomp }.join("\n")}
                #{nesting}#{editing ? "<div class='configuration-item-actions'><button class='button configuration-item-add'>Add Item</button></div>" : ""}</ul>
              CONFIGGROUP
            elsif schema.is_a?(Hash) && !["label", "revealedBy", "hiddenBy", "enabledBy", "disabledBy"].include?((key || "").to_s)
              # Output group of fields recursively (<li><ul> ... </ul></li>)

              # Determine the group id for this group
              groupId = id
              b64key = Base64.urlsafe_encode64((key || "").to_s)
              if key
                groupId = "#{groupId}[#{b64key}]"
              end

              # Get the editor widget for this item, if needed
              editor = ""
              if editing
                editor = self.renderConfigurationFieldEditor(schema, nesting + "  ", key, id)
              end

              # Whether or not the group is already expanded depends on its
              # current depth. The first groups are expanded and others are
              # collapsed by default.
              expanded = true

              # Get the enabled-by, etc, fields
              enables = self.renderConfigurationFieldEnables(schema)

              <<~CONFIGGROUP
                #{nesting}<li data-key='#{b64key}' #{enables}class='configuration-item configuration-item-group#{expanded ? " expanded" : ""}'>
                #{nesting}  #{self.renderConfigurationHeader(schema, key, editing).chomp}
                #{nesting}  <ul class='configuration-group'>
                #{schema.map { |k, v| self.renderConfigurationSchema(v, (data || {})[k], k, nesting + "    ", groupId, editing).chomp }.join("\n")}
                #{nesting}  #{editing ? "<div class='configuration-item-actions'><button class='button configuration-item-add'>Add Item</button></div>" : ""}</ul>
                #{nesting}</li>#{editor}
              CONFIGGROUP
            else
              ""
            end
          else
            ""
          end
        end

        # Renders the header of a configuration group.
        #
        # This assumes that the indicated `schema` is a group of configuration
        # items.
        def renderConfigurationHeader(schema, key = nil, editing = false)
          if key
            <<~CONFIGHEADER
              <h2><span class='label'>#{schema[:label] || key}</span>#{editing ? "<button class='expand-editor' title='#{I18n.t('configurations.expandEditor')}'>#{svg(:ui, :edit)}</button>" : ""}</h2>
            CONFIGHEADER
          else # We do not render a header for the initial group
            ""
          end
        end

        # Renders a configuration field.
        #
        # This assumes that the indicated `schema` describes a configuration
        # input field.
        def renderConfigurationField(schema, data = nil, key = nil, nesting = "", id = nil, editing = false)
          # Just in case we need a default prefix
          id ||= "data"

          # Get field input field
          field = self.renderConfigurationFieldInput(schema, data, key, nesting, id, editing)

          # Get the enabled-by, etc, fields
          enables = self.renderConfigurationFieldEnables(schema)

          # Determine the actionable type
          labelType = schema[:type]
          if schema[:type].is_a?(Array) || schema[:type] == :target
            labelType = "enum"
          end

          # Get field description
          # If we are 'editing' the schema, always ensure a description exists.
          if !schema[:description] && editing
            schema[:description] = ""
          end
          description = self.renderConfigurationFieldDescription(schema)

          # Get the editor widget for this item, if needed
          editor = ""
          if editing
            editor = self.renderConfigurationFieldEditor(schema, nesting, key, id)
          end

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)
          b64type = Base64.urlsafe_encode64((schema[:type] || "").to_json)

          # Whether or not we need a nullify field.
          #
          # The presense of a field with the 'nullify' name and the key will
          # remove that field when the value is blank. The configuration
          # widget could, in theory, turn this field off to reflect the
          # empty string is the value.
          #
          # We allow all fields to be 'nulled', but this could be omitted and
          # such a field would retain the empty string result. This is true
          # only for 'string' fields, which may want an empty string.
          #
          # For strings to have "" (empty string) as a value, they need to have
          # the empty string as a default. Then, when the configuration value is
          # nulled, the resulting field in the realized configuration will then
          # be an empty string. But then, null entries cannot have a default!
          nullify = "<input name='nullify#{id[4...]}[#{b64key}]' value='on' type='hidden' hidden></input>"

          datatype = "<input name='datatype#{id[4...]}[#{b64key}]' value='#{CGI::escapeHTML(labelType)}' type='hidden' hidden></input>"

          # Render it
          <<~CONFIGFIELD
            #{nesting}<li data-key="#{b64key}" data-type="#{b64type}" #{enables}class='configuration-item'>
            #{nesting}  <div class='label'><label data-type='#{labelType}' #{schema[:description] ? "" : "class='no-description'"}>#{schema[:label] || key}</label>#{schema[:description] ? "<button class='expand' title='#{I18n.t('configurations.expandDescription')}'>#{svg(:ui, :info)}</button>" : ""}#{editing ? "<button class='expand-editor' title='#{I18n.t('configurations.expandEditor')}'>#{svg(:ui, :edit)}</button>" : ""}<span class='configuration-dots' role='presentation'></span>#{description}</div><div class='value'>#{nullify}#{datatype}#{field.strip}</div>
            #{nesting}</li>#{editor}
          CONFIGFIELD
        end

        # Renders a configuration field editor.
        #
        # The `schema` refers to an existing item to pre-set the options.
        #
        # The editor contains dynamic sections to pick the various item schema
        # options, such as the type, label, etc.
        def renderConfigurationFieldEditor(schema, nesting = "", key = "", id = nil)
          types = ['string', 'int', 'date', 'datetime', 'time', 'number',
                   'group', 'boolean', 'color', 'enum', 'array']
          # TODO: 'enum' type shows a tagify field for the different values
          # TODO: 'array' types need a bevy of options to add fields
          id = "item-editor-field-#{(Random.rand * 10000000000000).round.to_s + "-" + key.to_s}"

          <<~CONFIGEDITOR
            #{nesting}<div class='configuration-item-editor' hidden>
            #{nesting}  <div class='editing-table'>
            #{nesting}    <div data-field='key' class='editing-table-field'>
            #{nesting}      <label for="#{id + "-key"}">#{"Key".downcase}</label><input id="#{id + "-key"}" value='#{key}'></input>
            #{nesting}    </div>
            #{nesting}    <div data-field='label' class='editing-table-field'>
            #{nesting}      <label for="#{id + "-label"}">#{"Label".downcase}</label><input id="#{id + "-label"}" value='#{schema[:label]}'></input>
            #{nesting}    </div>
            #{nesting}    <div data-field='type' class='editing-table-field'>
            #{nesting}      <label for="#{id + "-type"}">#{"Type".downcase}</label><select id="#{id + "-type"}">#{types.map{|x| "<option#{(x == schema[:type] || x == "enum" && schema[:type].is_a?(Array) || x == "group" && !schema[:type]) ? " selected" : ""}>#{x}</option>" }}</select>
            #{nesting}    </div>
            #{nesting}    <div data-field='items' class='editing-table-field'#{schema[:type].is_a?(Array) ? "" : " hidden"}>
            #{nesting}      <label for="#{id + "-items"}">#{"Items".downcase}</label><input id="#{id + "-items"}" class='tagify tags' value='#{schema[:type].is_a?(Array) ? schema[:type].join(',') : ''}'></input>
            #{nesting}    </div>
            #{nesting}    <div data-field='element' class='editing-table-field'#{schema[:type] == "array" ? "" : " hidden"}>
            #{nesting}      <label for="#{id + "-element"}">#{"Element Type".downcase}</label><select id="#{id + "-element"}" class='selector'>#{(types + ['group']).map{|x| "<option#{(x == schema[:element] || x == "enum" && schema[:element].is_a?(Array) || x == "group" && schema[:element].is_a?(Hash) ) ? " checked selected" : ""}>#{x}</option>" }}</select>
            #{nesting}    </div>
            #{nesting}    <div data-field='default' class='editing-table-field'#{(schema[:type] == "array" || !schema[:type]) ? " hidden" : ""}>
            #{nesting}      <label for="#{id + "-default"}"> #{"Default".downcase}</label><input id="#{id + "-default"}" value='#{schema[:default]}'></input>
            #{nesting}    </div>
            #{nesting}    <div data-field='units' class='editing-table-field'#{(schema[:type] == "array" || !schema[:type] || schema[:type].is_a?(Array)) ? " hidden" : ""}>
            #{nesting}      <label for="#{id + "-units"}">#{"Units".downcase}</label><input id="#{id + "-units"}" value='#{schema[:units]}'></input>
            #{nesting}    </div>
            #{nesting}  </div>
            #{nesting}  #{partial(:"objects/metadata/markdown-editor", :locals => { :data => schema[:description], :id => id + "-description", :name => nil, :active => false })}
            #{nesting}  <button class='button red configuration-item-delete'>Remove</button>
            #{nesting}</div>
          CONFIGEDITOR
        end

        # Renders the editable fields for the given configuration item schema.
        def renderConfigurationFieldInput(schema, data = nil, key = nil, nesting = "", id = nil, editing = false)
          # Just in case we need a default prefix
          id ||= "data"

          case schema[:type]
          when 'int', 'number'
            self.renderConfigurationFieldInteger(schema, data, key, id)
          when 'color'
            self.renderConfigurationFieldColor(schema, data, key, id)
          when 'date', 'datetime', 'time'
            self.renderConfigurationFieldDate(schema, data, key, id)
          when 'boolean'
            self.renderConfigurationFieldBoolean(schema, data, key, id)
          when 'array'
            self.renderConfigurationFieldArray(schema, data, key, nesting, id, editing)
          when 'tuple'
            self.renderConfigurationFieldTuple(schema, data, key, nesting, id, editing)
          when :target
            self.renderConfigurationFieldTarget(schema, data, key, id)
          when Array
            self.renderConfigurationFieldEnum(schema, data, key, id)
          else # 'string' is the default rendered type (an unbounded text input)
            self.renderConfigurationFieldString(schema, data, key, id)
          end
        end

        # Renders an integer configuration field.
        #
        # {
        #   "label": "Number of Things",
        #   "type": "int",
        #   "default": -1,
        #   "max": 5,
        #   "min": -1
        # }
        #
        # @param schema [Hash] The configuration schema defining the item.
        # @param data [String] The current value, if any, of the item.
        # @param key [String] The key for this item.
        # @param id [String] The encoded name for this item.
        #
        # @return [String] The rendered HTML representing this item.
        def renderConfigurationFieldInteger(schema, data = nil, key = nil, id = nil)
          # Gather the configuration field cues and attributes
          defaults = self.renderConfigurationFieldDefaults(schema)
          units = self.renderConfigurationFieldUnits(schema)

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)

          # Encode the type
          b64type = Base64.urlsafe_encode64((schema[:type] || "boolean").to_json)

          <<~CONFIGFIELD
            <span class='number'><input class='configuration-input' data-type='#{b64type}' #{defaults}name='#{id}[#{b64key}]' #{units}title='#{schema[:label] ? schema[:label] : (key)}#{schema[:units] ? " (#{schema[:units]})" : ""}' value='#{data || ''}' class='#{schema[:type]}'></span>
          CONFIGFIELD
        end

        # Renders an 'array' configuration field where elements are complex.
        #
        # {
        #   "label": "Items",
        #   "type": "array",
        #   "element": {
        #     "item1": {
        #       "label": "First Value",
        #       "type": "int",
        #       "default": 2
        #     },
        #     "item2": {
        #       "label": "Second Value",
        #       "type": "string",
        #       "default": "hello"
        #     }
        #   }
        # }
        def renderConfigurationArray(schema, data = nil, key = nil, nesting = "", id = nil, editing = false)
          # The array 'acts' like a group where the label is a header for a
          # section.

          # We want to render and current items
          current = ""

          current = (data || []).map do |value|
            "<li>#{self.renderConfigurationSchema(schema[:element], value, key, id, editing)}</li>"
          end.join("")

          represented = ""
          if editing
            represented = <<~CONFIGARRAYEDITOR
              #{nesting}  <li class='configuration-array-item'>
              #{self.renderConfigurationSchema(schema[:element], nil, nil, nesting + "    ", id, editing)}
              #{nesting}  </li>
            CONFIGARRAYEDITOR
          end

          # Complex arrays are never expanded by default.
          expanded = false

          # Determine the group id for this group
          groupId = id
          b64key = Base64.urlsafe_encode64((key || "").to_s)
          if key
            groupId = "#{groupId}[#{b64key}]"
          end

          # We want to render an empty set of inputs used to create a new item
          # This will be as a <template>
          template = <<~CONFIGARRAYTEMPLATE
            #{nesting}  <template>
            #{nesting}    <li class='configuration-array-item'>
            #{self.renderConfigurationSchema(schema[:element], nil, nil, nesting + "      ", id, editing)}
            #{nesting}    </li>
            #{nesting}  </template>
          CONFIGARRAYTEMPLATE

          b64type = Base64.urlsafe_encode64((schema[:type] || "array").to_json)

          # Get field description
          description = self.renderConfigurationFieldDescription(schema)

          # Get the editor widget for this item, if needed
          editor = ""
          if editing
            editor = self.renderConfigurationFieldEditor(schema, nesting, key, id)
          end

          <<~CONFIGARRAY
            #{nesting}<li data-type='#{b64type}' data-key='#{b64key}' class='configuration-item configuration-item-array configuration-item-group#{expanded ? " expanded" : ""}'>
            #{nesting}  #{self.renderConfigurationHeader(schema, key).chomp}
            #{nesting}  #{template}
            #{nesting}  <ul class='configuration-array-items'>
            #{represented}
            #{(data || []).each_with_index.map { |v, i|
              i = "-" + i.to_s
              groupId = id
              b64key = Base64.urlsafe_encode64((key || "").to_s + "(" + i.to_s + ")")
              if key
                groupId = "#{groupId}[#{b64key}]"
              end
              <<~CONFIGARRAYITEM
                #{nesting}    <li class='configuration-array-item'>
                #{self.renderConfigurationSchema(schema[:element], v, nil, nesting + "      ", groupId, editing).chomp}
                #{nesting}    </li>
              CONFIGARRAYITEM
            }.join("\n")}
            #{nesting}  </ul>
            #{nesting}  <div class='configuration-array-actions'>
            #{nesting}    <div class='label'><label #{schema[:description] ? "" : "class='no-description'"}>#{I18n.t('configurations.arrayLabel')}</label>#{schema[:description] ? "<button class='expand' title='#{I18n.t('configurations.expandDescription')}'>#{svg(:ui, :info)}</button>" : ""}#{editing ? "<button class='expand-editor' title='#{I18n.t('configurations.expandEditor')}'>#{svg(:ui, :edit)}</button>" : ""}<span class='configuration-dots' role='presentation'></span>#{description}</div><div class='value'><button class='button array-add'#{editing ? " disabled" : ""}>+</button></div>
            #{nesting}  </div>
            #{nesting}</li>#{editor}
          CONFIGARRAY
        end

        # Renders a 'tuple' configuration field.
        #
        # Tuples have multiple inputs associated with the same key.
        #
        # {
        #   "label": "Memory Size",
        #   "type": "tuple",
        #   "elements": [
        #     {
        #       "default": 2048,
        #       "type": "int"
        #     },
        #     {
        #       "default": "MB"
        #       "type": ["MB", "GB"]
        #     }
        #   ]
        # }
        def renderConfigurationFieldTuple(schema, data = nil, key = nil, nesting = "", id = nul, editing = false)
          # The data should be an array
          if !data.is_a?(Array)
            data = nil
          end

          current = (schema[:elements] || []).each_with_index.map do |element, i|
            # Data will be an array of values for each item in the schema's
            # elements list.
            value = nil
            if i < (data || []).count
              value = data[i]
            end

            # Render the element
            "<li class='configuration-tuple-item'><div class='value'>#{self.renderConfigurationFieldInput(element, value, key.to_s + "(" + i.to_s + ")", nesting + "  ", id, editing)}</div></li>"
          end.join("")

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)

          # We need a nullifying value for the entire tuple such that
          # empty/removed items actually disappear when using normal
          # form submissions.
          #
          # This is an actual data value that will force remove the tuple array
          # which then causes the element items to re-add on their own.
          nullify = "<input type='hidden' hidden name='#{id}[#{b64key}]' value=''></input>"

          # Render template, nullifier, current items, add button to add new item
          <<~CONFIGTUPLE
            #{nullify}<ul class='configuration-tuple-items'>#{current.strip}</ul>
          CONFIGTUPLE
        end

        # Renders an 'array' configuration field where elements have one type.
        #
        # {
        #   "label": "Items",
        #   "type": "array",
        #   "element": "string"
        # }
        def renderConfigurationFieldArray(schema, data = nil, key = nil, nesting = "", id = nil, editing = false)
          # We want to render and current items
          current = ""

          # Do a shallow clone to replace :type with :element and render it
          schema = schema.clone
          schema[:type] = schema[:element]
          schema[:label] = schema[:label] || key

          # Render the set of existing items as the given type
          if !data.is_a?(Array)
            data = nil
          end

          current = (data || []).each_with_index.map do |value, i|
            i = "-" + i.to_s
            "<li class='configuration-array-item'><div class='value'>#{self.renderConfigurationFieldInput(schema, value, key.to_s + "(" + i.to_s + ")", nesting + "  ", id, editing)}<button class='configuration-array-delete'>#{svg(:ui, :remove)}</button></div></li>"
          end.join("")

          # We want to render an empty set of inputs used to create a new item
          # This will be as a <template>
          template = "<template><li class='configuration-array-item'><div class='value configuration-array-value'>#{self.renderConfigurationFieldInput(schema, nil, key, nesting + "  ", id, editing)}<button class='configuration-array-delete'>#{svg(:ui, :remove)}</button></div></li></template>"

          # Encode the array element type
          b64type = Base64.urlsafe_encode64((schema[:element] || "").to_json)

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)

          # We need a nullifying value for the entire array such that
          # empty/removed items actually disappear when using normal
          # form submissions.
          #
          # This is an actual data value that will force remove the array and
          # then the items will re-add the array back.
          nullify = "<input type='hidden' hidden name='#{id}[#{b64key}]' value=''></input>"

          # Render template, nullifier, current items, add button to add new item
          <<~CONFIGSIMPLEARRAY
            #{template}#{nullify}<ul class='configuration-array-items' data-type='#{b64type}'>#{current.strip}</ul><button class='button array-add'#{editing ? " disabled" : ""}>+</button>
          CONFIGSIMPLEARRAY
        end

        def renderConfigurationFieldTarget(schema, data = nil, key = nil, id = nil)
          # Special cases for deployment configurations
          schema = schema.clone

          schema[:type] = [{
            :name => "Workflow default",
            :key => :__default__
          }]

          Occam::Job.targets(:status => true).each do |key, targetInfo|
            # Ignore :local
            next if key == :local

            schema[:type] << {
              :name => targetInfo[:name],
              :key => key
            }
          end

          self.renderConfigurationFieldEnum(schema, data, key, id)
        end

        # Render a configuration option that has just two possible values.
        def renderConfigurationFieldBoolean(schema, data = nil, key = nil, id = nil)
          # Gather the configuration field cues and attributes
          defaults = self.renderConfigurationFieldDefaults(schema)
          units = self.renderConfigurationFieldUnits(schema)

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)

          # Encode the type
          b64type = Base64.urlsafe_encode64((schema[:type] || "boolean").to_json)

          # The default is actually used in lieu of any unset data
          # No tri-bools here!
          if data.nil?
            data = schema[:default]
          end

          # Render Checkbox
          <<~CONFIGFIELD
            <div class='slider-checkbox'><span class='configuration-label-boolean configuration-label-boolean-off'>off</span><input class='slider-input' type='hidden' name='#{id}[#{b64key}]' value='off'><input class='configuration-input' data-type='#{b64type}' title='#{schema[:label] ? schema[:label] : (key)}' #{defaults}type='checkbox' name='#{id}[#{b64key}]'#{data == true ? " checked" : ""}><div class='label'></div><span class='configuration-label-boolean configuration-label-boolean-on'>on</span></div>
          CONFIGFIELD
        end

        # Renders a dropdown listing.
        #
        # When the type is an array, this will be rendered as a dropdown list.
        #
        # {
        #   "type": ["item 1", "item 2"]
        # }
        #
        # These can be just an array of possible strings or a more complicated
        # structure where the strings are encoded with keys. The 'key' is then
        # the value assigned when the selected string is chosen.
        #
        # {
        #   "type": [
        #     {
        #       "key": "item1",
        #       "name": "Item 1"
        #     },
        #     {
        #       "key": "item2",
        #       "name": "Item 2 (Advanced)"
        #     }
        #   ]
        # }
        #
        # @param schema [Hash] The configuration schema defining the item.
        # @param data [String] The current value, if any, of the item.
        # @param key [String] The key for this item.
        # @param id [String] The encoded name for this item.
        #
        # @return [String] The rendered HTML representing this item.
        def renderConfigurationFieldEnum(schema, data = nil, key = nil, id = nil)
          # Gather the configuration field cues and attributes
          defaults = self.renderConfigurationFieldDefaults(schema)
          units = self.renderConfigurationFieldUnits(schema)

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)

          # Render dropdown
          <<~CONFIGFIELD
            <div class='select'><select class='configuration-input' name='#{id}[#{b64key}]'>#{schema[:type].map { |sub_type|
              subKey = sub_type
              subLabel = sub_type
              if sub_type.is_a? Hash
                subKey = sub_type[:key]
                subLabel = sub_type[:name] || subKey
              end
              selected = (data == subKey)
              "<option#{selected ? " selected='selected'" : ""} value='#{subKey}'>#{subLabel}#{(subKey != :__default__ ? " [#{subKey}]" : "")}</option>"
            }.join("")}</select></div>
          CONFIGFIELD
        end

        def renderConfigurationFieldColor(schema, data = nil, key = nil, id = nil)
          # Gather the configuration field cues and attributes
          defaults = self.renderConfigurationFieldDefaults(schema)
          units = self.renderConfigurationFieldUnits(schema)

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)

          # Encode the type
          b64type = Base64.urlsafe_encode64((schema[:type] || "color").to_json)

          # Render color picker
          <<~CONFIGFIELD
            <span class='number color'><input class='configuration-input configuration-input-color' data-type='#{b64type}' #{defaults}name='#{id}[#{b64key}]' #{units}title='#{schema[:label] ? schema[:label] : (key)}#{schema[:units] ? " (#{schema[:units]})" : ""}' value='#{data || ''}'></span>
          CONFIGFIELD
        end

        def renderConfigurationFieldDate(schema, data = nil, key = nil, id = nil)
          # Gather the configuration field cues and attributes
          defaults = self.renderConfigurationFieldDefaults(schema)
          units = self.renderConfigurationFieldUnits(schema)

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)

          # Encode the type
          b64type = Base64.urlsafe_encode64((schema[:type] || "datetime").to_json)

          inputtype = "datetime-local"
          if schema[:type] == "date"
            inputtype = "date"
          elsif schema[:type] == "time"
            inputtype = "time"
          end

          # Render date/time picker
          <<~CONFIGFIELD
            <span class='number date'><input class='configuration-input configuration-input-date' data-type='#{b64type}' #{defaults}name='#{id}[#{b64key}]' #{units}title='#{schema[:label] ? schema[:label] : (key)}#{schema[:units] ? " (#{schema[:units]})" : ""}' value='#{data || ''}' type='#{inputtype}' step='1'></span>
          CONFIGFIELD
        end

        def renderConfigurationFieldString(schema, data = nil, key = nil, id = nil)
          # Gather the configuration field cues and attributes
          defaults = self.renderConfigurationFieldDefaults(schema)
          units = self.renderConfigurationFieldUnits(schema)

          # Get the encoded identifier
          b64key = Base64.urlsafe_encode64((key || "").to_s)

          # Encode the type
          b64type = Base64.urlsafe_encode64((schema[:type] || "string").to_json)

          <<~CONFIGFIELD
            <span class='number'><input class='configuration-input' data-type='#{b64type}' #{defaults}name='#{id}[#{b64key}]' #{units}title='#{schema[:label] ? schema[:label] : (key)}#{schema[:units] ? " (#{schema[:units]})" : ""}' value='#{data || ''}'></span>
          CONFIGFIELD
        end

        # Renders the configuration description within the schema.
        #
        # By default, the schema description is considered markdown.
        #
        # {
        #   "type": "string",
        #   "description": "This is **markdown** flavored text."
        # }
        def renderConfigurationFieldDescription(schema)
          # TODO: add the defaults and validations
          validations = self.renderConfigurationFieldValidations(schema)

          description = ""
          if schema.has_key? :description
            engine = ::Redcarpet::Markdown.new(Redcarpet::Render::HTML, {})
            description = engine.render(schema[:description])
          end

          validations = self.renderConfigurationFieldValidations(schema)

          "<div class='description' hidden><div class='description-content'>#{description}</div><div class='no-description none empty' aria-hidden='true'><p>#{I18n.t('configurations.noDescription')}</p></div><ul class='configuration-validations'>#{validations}</ul><ul class='errors'></ul></div>"
        end

        # Determines attributes to add to fields based on desired validations.
        def renderConfigurationFieldValidations(schema)
          tests = []
          messages = []

          case schema[:type]
          when 'int'
            tests.push("isInteger(x)")
            messages.push(I18n.t('configurations.validations.typeError', :value => schema[:type]))
          when 'number'
            tests.push("isNumber(x)")
            messages.push(I18n.t('configurations.validations.typeError', :value => schema[:type]))
          end

          (schema[:validations] || []).each do |validation|
            if validation.has_key? :min
              # Add default message
              toPush = {}
              toPush[:message] = validation[:message] || I18n.t('configurations.validations.minError', :value => validation[:min])
              toPush[:test] = "x >= #{validation[:min]}"

              tests.push(toPush[:test])
              messages.push(toPush[:message])
            end

            if validation.has_key? :max
              # Add default message
              toPush = {}
              toPush[:message] = validation[:message] || I18n.t('configurations.validations.maxError', :value => validation[:max])
              toPush[:test] = "x <= #{validation[:max]}"

              tests.push(toPush[:test])
              messages.push(toPush[:message])
            end

            if validation.has_key? :test
              toPush = {}
              toPush[:message] = validation[:message] || I18n.t('configurations.validations.testError', :value => validation[:test])
              toPush[:test] = validation[:test]

              tests.push(toPush[:test])
              messages.push(toPush[:message])
            end
          end

          # For each test/message, add a validation element
          tests.zip(messages).map do |test, message|
            "<li class='configuration-validation' data-test='#{Base64.urlsafe_encode64(test.to_s)}'>#{svg(:ui, :test)}#{svg(:ui, :error, :class => "error")}<div class='configuration-validation-message'><p>#{message}</p></div></li>"
          end.join('')
        end

        # Determines attributes to add to fields based on desired show/hide/etc.
        def renderConfigurationFieldEnables(schema)
          # We will return the enables string
          ret = ""

          # The enabledBy key is a set of keys that have to be fulfilled in
          # order to enable this option.
          if schema.has_key? :enabledBy
            b64token = Base64.urlsafe_encode64(schema[:enabledBy].to_json)
            ret += "data-enabled-by=\"#{b64token}\" "
          end

          # The revealedBy key is a set of keys that have to be fulfilled in
          # order to enable this option.
          if schema.has_key? :revealedBy
            b64token = Base64.urlsafe_encode64(schema[:revealedBy].to_json)
            ret += "data-revealed-by=\"#{b64token}\" "
          end

          # The disabledBy key is a set of keys that when they are fulfilled,
          # the item is disabled.
          if schema.has_key? :disabledBy
            b64token = Base64.urlsafe_encode64(schema[:disabledBy].to_json)
            ret += "data-disabled-by=\"#{b64token}\" "
          end

          # The hiddenBy key is a set of keys that when they are fulfilled,
          # the item is hidden.
          if schema.has_key? :hiddenBy
            b64token = Base64.urlsafe_encode64(schema[:hiddenBy].to_json)
            ret += "data-hidden-by=\"#{b64token}\" "
          end

          ret
        end

        # Determines the attributes to add to encode the default value.
        def renderConfigurationFieldDefaults(schema)
          defaults = ""
          if schema[:default]
            defaults += "data-default='#{Base64.urlsafe_encode64(schema[:default].to_json)}' "
          end

          defaults
        end

        # Determines the attributes to add to encode the units for the field.
        def renderConfigurationFieldUnits(schema)
          units = ""
          if schema[:units]
            units = "data-units=\"#{Base64.urlsafe_encode64(schema[:units])}\" "
          end

          units
        end
      end
    end

    helpers Helpers::ConfiguratorHelpers
  end
end

