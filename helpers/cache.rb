class Occam
  class Controller
    module Helpers
      module CacheHelpers
        def no_cache
          headers "Expires"       => "Fri, 01 Jan 1980 00:00:00 GMT",
                  "Pragma"        => "no-cache",
                  "Cache-Control" => "no-cache, max-age=0, must-revalidate"
        end

        def forever_cache
          now = Time.now
          headers "Date" => now.to_s,
                  "Expires" => (now + 31536000).httpdate,
                  "Cache-Control" => "public, max-age=31536000"
        end
      end
    end

    helpers Helpers::CacheHelpers
  end
end
