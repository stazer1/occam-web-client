# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  class Controller
    module Helpers
      # This module implements encrypted_token, cookie_encrypter, set_token, and get_token
      module TokenHelpers
        def cookie_encrypter
          Rack::Session::EncryptedCookie::Encryptor.new(Occam::Config.configuration['secret'])
        end

        def encrypt_token(object=nil)
          payload = {}
          if object && object.tokens && object.tokens.any?
            payload = {:token => object.tokens[0],
                       :identity => object.identity.uri,
                       :object => object.baseID}
          elsif current_account
            payload = {:token => current_account.token, :identity => current_account.identity}

            if object
              payload[:object] = object.baseID
            end
          end

          cookie_encrypter.encrypt(payload.to_json)
        end

        def decrypt_token(token, object=nil)
          payload = JSON.parse(cookie_encrypter.decrypt(token), :symbolize_names => true)
          if (payload[:object] && object.nil?) || (payload[:object] && payload[:object] != object.baseID)
            {}
          else
            payload
          end
        rescue
          {}
        end
      end
    end

    helpers Helpers::TokenHelpers
  end
end
