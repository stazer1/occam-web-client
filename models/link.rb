# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # Represents a Link (and associated object)
  class Link
    # The source Object
    attr_reader :source

    # The target Object
    attr_reader :target

    # The link relationship which denotes the context of the link
    attr_reader :relationship

    # The internal id for the link which is used to update/delete it
    attr_reader :id

    def initialize(options)
      @source       = options[:source]
      @target       = options[:target]
      @relationship = options[:relationship]
      @account      = options[:account]
      @id           = options[:id]
    end

    def destroy!
      @source.destroyLink!(self)
    end
  end
end
