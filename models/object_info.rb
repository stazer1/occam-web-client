class Occam
  # Represents the object metadata.
  #
  # This also has methods that help convert POST data into the appropriate
  # form needed to pass to a `set` command.
  class ObjectInfo
    # Creates a new ObjectInfo data object.
    def initialize
    end

    # Converts strings in a hash that start with 'JSON:' into hashes.
    #
    # It does this, of course, by parsing the content after the colon as JSON.
    def self.reencode(hash)
      hash.each do |key, value|
        if value.is_a?(Hash)
          reencode(value)
        else
          if value.is_a?(String) && value.start_with?("JSON:")
            hash[key] = JSON.parse(value[5..-1])
          end
        end
      end
    end

    def self.removeNullify(hash, nullify)
      hash.each do |key, value|
        if nullify[key] && !nullify[key].is_a?(Hash) && value == ""
          hash.delete(key)
        elsif nullify[key] && value.is_a?(Hash)
          ObjectInfo.removeNullify(value, nullify[key])
        end
      end
    end

    # Returns a set of 'items' from the given hash.
    #
    # The 'items' it returns are in the form of tuples like those expected by
    # `set` commands.
    def self.itemsFromHash(hash, nullify = nil, datatype = nil, rewrite = nil, prefix = nil)
      # Default nullify/datatype/rewrite to empty hashes
      nullify ||= {}
      datatype ||= {}
      rewrite ||= {}

      # The goal is to produce a set of items tuples
      items = []

      # We do this by going through every entry in the provided hash object
      hash.each do |key, value|
        # Fix array references
        # Anything that references array index '-' will become just '[]' which
        # will append. This allows forms to easily append multiple items but
        # give them unique names
        canonicalKey = key.gsub(/\([-][^)]+\)/, '()')

        # Now replace the () with the proper [] (which we couldn't use within
        # keys because that's how we parse string subkeys for group hierarchies
        # instead via the params block)
        canonicalKey = canonicalKey.gsub('(', '[').gsub(')', ']')

        # Prepend the group prefix to craft the overall key
        if prefix
          canonicalKey = prefix + "." + canonicalKey
        end

        # Rewrite keys, if necessary
        rewrite.each do |k, v|
          canonicalKey.gsub!(k, v)
        end

        if nullify[key] && value == ""
          # When it matches a nullify key, return an item tuple that deletes it
          items << [canonicalKey]
        elsif datatype[key] == "tuple"
          items << [canonicalKey, value.to_json]
        elsif datatype[key] == "boolean"
          # Coerce data ('on', 'off') to a boolean
          value = (value == "on")
          items << [canonicalKey, value.to_json]
        elsif datatype[key] == "int"
          # Coerce data to an integer, maybe
          if value.to_i.to_s == value.to_s
            value = value.to_i
          end
          items << [canonicalKey, value.to_json]
        elsif datatype[key] == "number"
          # Coerce data to an integer
          if value.to_f.to_s == value.to_s
            value = value.to_f
          end
          items << [canonicalKey, value.to_json]
        elsif datatype[key] == "list"
          # This creates an array with values that are set ('on')
          # This is used to interpret checkbox values that form an array
          #   in the metdata.
          if value.is_a?(Hash)
            value = value.filter{|_,v| v == "on"}.map{|k,v| k}
          end
          items << [canonicalKey, value.to_json]
        elsif datatype[key] == "tags"
          # This converts data that is supplied by a tags input field,
          #   which is annoyingly a weird data format for no good reason.
          items << [canonicalKey, JSON.parse(value).map{|i| i["value"]}.to_json]
        else
          if value.is_a?(Hash) && !key.end_with?("()")
            # Recurse, actually
            items.concat(ObjectInfo.itemsFromHash(value, nullify[key] || {}, datatype[key] || {}, rewrite[key] || {}, canonicalKey))
          else
            # We want to append a dict, so we encode the entire thing as a json
            items << [canonicalKey, value.to_json]
          end
        end
      end

      items
    end

    def self.parseParams(params)
      data = params[:data] || {}

      # If the data is not a Hash, it is JSON, parse it
      if !data.is_a?(Hash)
        begin
          data = JSON.parse(data)
        rescue
          data = {}
        end
      end

      # Some values in the hash are meant to encode JSON, so look for those
      ObjectInfo.reencode(data)

      nullify = params[:nullify] || {}
      datatype = params[:datatype] || {}
      rewrite = params[:rewrite] || {}

      # Parse the data hash and return the items array
      items = ObjectInfo.itemsFromHash(data, nullify, datatype, rewrite)
    end
  end
end
