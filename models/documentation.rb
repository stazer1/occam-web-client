# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # Represents an Occam documentation page.
  class Documentation
    CARD = "<div class='card markdown'>"

    attr_reader :filename
    attr_reader :name

    class Renderer < Redcarpet::Render::HTML
      def codespan(code)
        "<code>#{CGI::escapeHTML(code).gsub(/\-/, "&#8209;")}</code>"
      end

      def block_code(code, language)
        require 'rouge'

        formatter = Rouge::Formatters::HTML.new
        lexer = Rouge::Lexer.find(language)
        if lexer.nil?
          lexer = Rouge::Lexer.guess_by_source(code)
        end
        "<pre><code class='highlight #{language}'>#{formatter.format(lexer.lex(code))}</code></pre>"
      end

      def link(link, title, content)
        options = content.match(/^(.*)\|/)
        options = options[1] if options

        content.gsub!(/^.*\|/, "")

        classes = []
        attributes = {}

        if options
          options.split('|').each do |option|
            key, value = option.split('=', 2)
            case key
            when "modal"
              classes << "modal"
            when /^data-.*/
              attributes[key] = value || true
            end
          end
        end

        attributes = attributes.map do |k, v|
          if v.is_a?(TrueClass)
            "#{k}"
          elsif not v.is_a?(FalseClass)
            "#{k}='#{v}'"
          end
        end.join(' ')

        "<a href='#{link}' class='#{classes.join(' ')}' #{attributes}>#{content}</a>"
      end

      def image(link, title, alt_text)
        require 'nokogiri'

        alt_text = alt_text || "!"

        options = alt_text.match(/^(.*)\|/)
        options = options[1] if options

        alt_text.gsub!(/^.*\|/, "")

        caption = ""
        caption = alt_text unless alt_text.start_with? "!"

        alt_text = alt_text[1..-1] if alt_text.start_with?("!")

        classes = "image"

        style = ""
        inline = false

        if options
          options.split('|').each do |option|
            key, value = option.split('=', 2)
            case key
            when "inline"
              inline = true
            when "border"
              classes << " border"
            when "right"
              classes << " right"
            when "left"
              classes << " left"
            when "width"
              style << "width: #{value};"
            when "padding"
              style << "padding: #{value};"
            when "alt"
              alt_text = value
            end
          end
        end

        caption = Redcarpet::Markdown.new(self.class.new).render(caption)
        alt_text = alt_text || caption
        alt_text = Nokogiri::HTML(alt_text).xpath("//text()").remove.to_s
        alt_text = CGI.escapeHTML(alt_text)

        img_source = "<img style='#{style}' src='#{link}' #{title ? "title='#{title}'" : ""} alt='#{alt_text}' />"

        if link.match "http[s]?://(www.)?youtube.com"
          # embed the youtube link
          youtube_hash = link.match("youtube.com/.*=(.*)$")[1]
          img_source = "<div class='yt'><div class='yt_fixture'><img style='#{style}' src='/images/yt_placeholder.png' /><iframe class='yt_frame' src='https://www.youtube.com/embed/#{youtube_hash}'></iframe></div></div>"
        end

        caption = "<br /><div class='caption'>#{caption}</div>" unless caption == ""

        if inline
          img_source
        else
          "</p><div class='#{classes}'>#{img_source}#{caption}</div><p>"
        end
      end
    end

    def self.list
      if !defined?(@@docs) || @@docs.nil?
        basepath = File.join(File.dirname(__FILE__), "..", "views", "documentation")

        @@docs = Dir[File.join(basepath, "*.md")].map do |path|
          next if path.end_with?("README.md")
          File.basename(path)
        end.compact.sort.map do |tag|
          Occam::Documentation.new(tag)
        end
      end

      @@docs.map(&:generateURLSlug).map(&:intern)
    end

    def self.load(tag)
      # Load and cache all documentation pages
      self.list

      # Get the document with the given tag
      @@docs.filter do |doc|
        doc.generateURLSlug.intern == tag.intern
      end.first
    end

    def initialize(filename, options = {})
      # Retain filename
      @filename = filename.to_s

      # Get the full path to the markdown
      @path = File.join(File.dirname(__FILE__), "..", "views", "documentation", @filename)

      # Cache a render of this document (which parses out the name and section titles)
      self.render
    end

    # Generates a slug for the given document's URL.
    def generateURLSlug
      @name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
    end

    # Generates a slug for the given subheader name.
    def generateSlug(name=nil)
      name = name || "_top_"

      "documentation-" +
      @name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '') + "-" +
        name.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
    end

    # Returns the set of subsections of this documentation page.
    def sections
      (@sections || []).dup
    end

    # Replaces special links in documentation to the appropriate routes.
    def replaceLinks(doc, options = {})
      # %occam-git%, %occam-client-git%: Occam git repositories for installing Occam
      if options[:request]
        request = options[:request]
        domain = "#{request.scheme}://#{request.host_with_port}"
      else
        domain = Occam.domain
      end
      doc.gsub!(/%%occam-git%%/, "#{domain}/system/code")
      doc.gsub!(/%%occam-client-git%%/, "#{domain}/system/client-code")

      doc
    end

    # Replaces special tags with rendered representations of their content.
    def replaceTags(doc, options={})
      doc.gsub(/%%(.*?)%%/) do |original|
        tag = Regexp.last_match[1]
        tag, arguments = tag.split("|", 2).map(&:strip)
        arguments = (arguments || "").split("|").map(&:strip)

        case tag
        when "navigation"
          "<span class='topbar documentation'><span class='navbar'><span class='item'><a href='#'>#{I18n.t("navigation.#{arguments.join('.')}")}</a></span></span></span>"
        when "tab"
          "<span class='card-tabs tabs documentation'><span class='tab'><a href='#'>#{I18n.t("#{arguments.join(".")}.header")}</a></span></span>"
        when "button"
          "<button class='documentation button inline'>#{I18n.t(arguments.join('.'))}</button>"
        when "header"
          "<code>#{I18n.t(arguments.join('.') + ".header")}</code>"
        else
          original
        end
      end
    end

    # Returns HTML representing the given section name.
    def render(options = {})
      options = options.update({:fenced_code_blocks => true})
      markdown = File.read(File.join(@path))
      engine = ::Redcarpet::Markdown.new(Occam::Documentation::Renderer, options)
      result = engine.render(markdown).strip

      # Acquire name
      @name = (result.match(/<h1>(.*)<\/h1>/) || ["","Unknown"])[1]

      # It is annoying the groups in scan are different than groups in match
      @sections = result.scan(/<h2>(.*?)<\/h2>/).map do |subheader|
        {
          :name => subheader[0],
          :slug => self.generateSlug(subheader[0])
        }
      end

      # Now, add the slugs to the headers
      @sections.each do |section|
        result.gsub!(/<h2>\s*#{Regexp.quote(section[:name])}\s*<\/h2>/, "<h2 id='#{section[:slug]}'>#{section[:name]}</h2>")
      end

      # Get rid of header
      result.gsub!(/<h1>.*<\/h1>/, '')

      # Turn h2's into separate cards

      result.gsub!(/<h2/, "</div>#{CARD}<h2")
      result.gsub!("<pre>", "</div><div class='card markdown'><pre>")
      result.gsub!("</pre>", "</pre></div>#{CARD}")

      # Get rid of empty cards
      result.gsub!(/#{CARD}\s*?<\/div>/, '')

      # Replace special tags
      result = self.replaceLinks(result, options)
      result = self.replaceTags(result, options)

      "#{CARD}#{result}</div>"
    end
  end
end
