# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'object'

class Occam
  # Represents an Occam object.
  class Template < Occam::Object
    require 'json'

    # Gets a list of Template Objects.
    def self.retrieveAll(options = {})
      Occam::Object.search(:account => options[:account],
                           :subtype => "object-template",
                           **options).map do |object|
        object.as(Occam::Template)
      end
    end
  end
end
