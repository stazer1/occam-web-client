# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This model represents the link between a Person and an Object
  class Authorship
    # The local record id for the authorship
    attr_reader :id

    # The Person object (if any) this authorship is attached to
    attr_reader :person

    # The Object this authorship is attached to
    attr_reader :object

    # The role (:author / :collaborator)
    attr_reader :role

    # The name of the person attached to this authorship
    attr_reader :name

    # The account that owns this authorship record
    attr_reader :account

    # Initializes an authorship record.
    # (Does not create an authorship. see #create)
    def initialize(options)
      @id      = options[:id]
      @role    = options[:role]
      @name    = options[:name]
      @person  = options[:person]
      @object  = options[:object]
      @account = options[:account]
    end

    def self.create(options)
      authorship = Occam::Authorship.new(options)

      cmdOptions = {}

      if @account
        cmdOptions["-T"] = authorship.account.token
      end

      cmdOptions["--role"] = authorship.role

      arguments = [
        authorship.person.fullID,
        authorship.object.fullID
      ]

      # Issue daemon command to create the authorship
      result = Occam::Worker.perform("accounts", "add", arguments, cmdOptions)
      info = JSON.parse(result[:data], :symbolize_names => true)
      Occam::Authorship.new(options.update(info))
    end

    # Detroys the authorship
    def destroy!
      cmdOptions = {}

      if @account
        cmdOptions["-T"] = self.account.token
      end

      cmdOptions["--role"] = self.role

      Occam::Worker.perform("accounts", "remove", [
        self.person.fullID,
        self.object.fullID
      ], cmdOptions)

      nil
    end
  end
end
