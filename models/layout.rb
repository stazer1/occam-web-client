# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This represents information about how we are rendering a page.
  class Layout
    # This describes the current rendering mode. Values include:
    #   :desktop  => A full rendering of the page
    #   :mobile   => Intended to render to a small device
    #   :embedded => Intended to render to an embedded container
    #   :minimal  => Shows only the tab pane or viewed object
    attr_reader :view

    # This describes the pagination on the current page.
    attr_reader :pagination

    # This denotes which tab on the main tab strip we are viewing.
    attr_reader :tab

    # This denotes which subtab on a secondary tab strip we are viewing, if any.
    attr_reader :subtab

    # This tells us which help option has been opened.
    #
    # Although the help bubbles on the site are generally opened via javascript,
    # there are times when they must be opened by a full page load when js is
    # disabled. This will help trigger those help bubbles on a render.
    attr_reader :help

    # The theme in use
    attr_reader :theme

    def initialize(options)
      @view       = options[:view] || :desktop
      @pagination = options[:pagination]
      @tab        = options[:tab]
      @subtab     = options[:subtab]
      @help       = options[:help]
      @modal      = options[:modal] || false
      @ajax       = options[:ajax] || false
      @webp       = options[:webp] || false
      @ie         = options[:ie] || false

      theme = options[:theme] || :nostalgia
      if !defined?(@@themes)
        @@themes = {}
      end

      if @@themes[theme].nil?
        @@themes[theme] = Occam::System::Theme.new(theme)
      end

      @theme = @@themes[theme]
    end

    # Whether or not this layout is meant to render to a desktop view.
    def desktop?
      !self.mobile
    end

    # Whether or not this layout is meant to render to a mobile view.
    def mobile?
      @view == :mobile
    end

    # Whether this renders an embedded view.
    def embedded?
      @view == :embedded
    end

    # Whether this renders a minimal view.
    def minimal?
      @view == :minimal
    end

    # Whether we are rendering a modal.
    def modal?
      @modal
    end

    # Whether or not the client accepts webp
    def webp?
      @webp
    end

    # Whether or not the browser is IE
    def ie?
      @ie
    end

    # Determines the layout markdown template to use
    def layout
      if @view == :minimal
        :minimal
      else
        !@ajax
      end
    end
  end
end
