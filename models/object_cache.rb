class Occam
  # Represents a cache of Object instantiations that can be cloned when an
  # Object is queried.
  #
  # There are several concerns. The first is that some Objects are only
  # accessible by certain accounts, so the cache might be tagged by Account.
  #
  # The other is that an Object in the cache may change. Much of the data in
  # the system is tagged by a revision and does not change. Some metadata that
  # does change are overall metadata including tags and descriptions.
  class ObjectCache
    # Creates a new Object cache.
    def initialize()
      @cache = {}
      @remoteCache = {}
    end

    # Retrieve an Object from the cache, if it exists, from a set of values.
    #
    # This will resolve a new Object if the cache misses.
    def resolve(options, as = Occam::Object)
      token = self.tokenFor(options)

      if options[:tokens] && options[:tokens].any?
        token = nil
      end

      if token.nil?
        # Cannot cache this
        as.new(options)
      elsif @cache[token]
        # Pull from cache
        @cache[token].as(as, {:cache => self,
                              :path => nil,
                              **options})
      elsif options[:account] && @cache[token + "-" + options[:account].token]
        @cache[token + "-" + options[:account].token].as(as, {:cache => self,
                                                              :path => nil,
                                                              **options})
      elsif @remoteCache[token]
        # Pull from cache
        @remoteCache[token].as(as, {:cache => self,
                                    :path => nil,
                                    **options,
                                    :remote => true,
                                    })
      else
        # Create and store in cache
        object = as.new(:cache => self,
                        **options)

        if object.exists? && object.cacheable?
          # Pull metadata
          object.info(true)

          # Store cached version
          if object.remote?
            @remoteCache[token] = object
          elsif object.public?
            @cache[token] = object
          elsif options[:account] && object.canView?
            @cache[token + "-" + options[:account].token] = object
          end
        end

        object
      end
    end

    # Returns the cache token that uniquely identifies an object with the
    # given keys.
    def tokenFor(object_or_hash)
      options = object_or_hash
      if !object_or_hash.is_a?(Hash)
        options = {:id       => object_or_hash.id,
                   :revision => object_or_hash.revision}

        if !object_or_hash.link.nil?
          options[:link] = object_or_hash.link
        end
      end

      ret = nil

      if options[:link].nil? && options[:id] && options[:revision]
        ret = options[:id] + "@" + options[:revision]

        if options[:index] && options[:index].any?
          ret = ret + "[" + options[:index].join('][') + "]"
        end
      end

      ret
    end

    # Removes, if it exists, the entry cached for the given object keys.
    def invalidate(object={})
      token = self.tokenFor(object)

      @cache.delete(token)
      if token && object.account && object.account.token
        @cache.delete(token + "-" + object.account.token)
      end
    end
  end
end
