require_relative '../lib/silence_warnings'

# Create a test occam root
specPath = File.join(File.dirname(__FILE__))
occamPath = File.join(specPath, ".occam")
emptyOccamPath = File.join(specPath, ".empty-occam")

# Ensure we always point to our nonsense Occam root just in case
ENV['OCCAM_ROOT'] = occamPath

# Clone/Create initial empty occam instance
if not File.exist?(emptyOccamPath)
  puts
  print "Initializing an occam instance... "
  `OCCAM_ROOT=#{emptyOccamPath} occam system initialize > /dev/null 2> /dev/null`

  # Copy a web config in
  FileUtils.cp(File.join(specPath, "web-config.yml"), File.join(emptyOccamPath, "web-config.yml"))

  FileUtils.copy_entry emptyOccamPath, occamPath
  puts "Done"
elsif not File.exist?(occamPath)
  # Clone the occam instance for this test run
  puts
  print "Cloning an empty occam instance... "
  FileUtils.copy_entry emptyOccamPath, occamPath
  puts "Done"
else
  puts
  print "Reusing occam instance... "
  puts "OK"
end

# Code Coverage
require 'simplecov'

SimpleCov.profiles.define 'occam' do
  add_filter 'spec/'
  add_filter 'config/'
  add_filter 'key/'

  add_group 'Controllers', 'controllers'
  add_group 'Models',      'models'
  add_group 'Helpers',     'helpers'
  add_group 'Libraries',   'lib'
  add_group 'Views',       'views'

  coverage_dir "spec/coverage"

  command_name "Minitest"
end

SimpleCov.start 'occam'

silence_warnings do
  require "rack/test"

  # Load the testing framework
  require 'minitest/spec'
  require 'minitest/reporters'

  # Our custom JSON reporter
  class JSONReporter < Minitest::Reporters::BaseReporter
    def initialize(options = {})
      @result = {}

      @result[:metadata] = {
        :ruby_version => RUBY_VERSION,
        :ruby_patchlevel => RUBY_PATCHLEVEL,
        :ruby_platform => RUBY_PLATFORM,
        :time => Time.now.utc.iso8601
      }

      @result[:statistics] = {
        :total      => 0,
        :assertions => 0,
        :failures   => 0,
        :errors     => 0,
        :skips      => 0,
        :passes     => 0
      }

      @result[:groups]     = {}

      @result[:groups][:models] = {
        :name => "Models",
        :describes => {}
      }

      @result[:groups][:helpers] = {
        :name => "Helpers",
        :describes => {}
      }

      @result[:groups][:libraries] = {
        :name => "Libraries",
        :describes => {}
      }

      @result[:groups][:acceptance] = {
        :name => "Acceptance",
        :features => {}
      }

      super
    end

    def record(result)
      # TODO: add the relative source file so we can link it
      test = {
        :name       => result.name,
        :class      => result.klass,
        :time       => result.time,
        :type       => result.passed? ? :passed : (result.skipped? ? :skipped : :failed),
        :assertions => result.assertions
      }

      if result.passed?
        @result[:statistics][:passes] += 1
      elsif result.skipped?
        @result[:statistics][:skips] += 1
      else
        @result[:statistics][:failures] += 1
      end

      @result[:statistics][:total] += 1
      @result[:statistics][:assertions] += (result.assertions || 0)

      test[:number] = test[:name].match(/^test_(\d+)_/)[1].to_i
      if test[:class].match(/:type\s*=>\s*:feature}/)
        feature = test[:class].gsub(/::{.*$/, "")
        test[:scenario] = test[:name].match(/^test_\d+_(.*)$/)[1]

        @result[:groups][:acceptance][:features][feature] ||= {
          :feature => feature,
        }
        @result[:groups][:acceptance][:features][feature][:tests] ||= []
        @result[:groups][:acceptance][:features][feature][:tests] << test
      else
        section = test[:class].match(/::{:type=>:([^}]+)}/)
        if section
          section = section[1]
        end
        test[:class] = test[:class].gsub(/::{:type[^}]+}/, "")
        sections = test[:class].split("::")
        sections = [sections[0..1].join("::")] + sections[2..-1]

        if section
          section = section.intern
        else
          section = :models
        end

        type = :class
        if section == :helper
          section = :helpers
          type    = :module
        elsif section == :library
          section = :libraries
          type    = :class
        end

        currentSection = @result[:groups][section]
        sections.each_with_index do |describe, i|
          currentSection[:describes] ||= {}
          currentSection = currentSection[:describes]
          currentSection[describe] ||= {}

          currentSection[describe][:name] = describe
          if i == 0
            currentSection[describe][:type] = type
          elsif i == 1
            if describe.start_with?("#")
              currentSection[describe][:type] = :method
            else
              currentSection[describe][:type] = :function
            end
          else
            currentSection[describe][:type] = :group
          end

          currentSection = currentSection[describe]
        end

        test[:it] = test[:name].match(/^test_\d+_(.*)$/)[1]
        currentSection[:tests] ||= []
        currentSection[:tests] << test
      end

      test.delete :class
      test.delete :name
    end

    def failures
      super || 0
    end

    def errors
      super || 0
    end

    def skips
      super || 0
    end

    def report
      puts "Generating a JSON report in `spec/tests.json'."

      @result[:metadata][:finished] = Time.now.utc.iso8601

      @result[:timings] = {
        :total_seconds         => total_time,
        :runs_per_second       => count / total_time,
        :assertions_per_second => assertions / total_time
      }

      specPath  = File.dirname(__FILE__)
      File.open(File.join(specPath, "tests.json"), "w+") do |of|
        of.write(JSON.pretty_generate(@result))
      end
    end
  end

  if ENV["OUTPUT"] == "SPEC"
    Minitest::Reporters.use! [Minitest::Reporters::SpecReporter.new, JSONReporter.new]
  else
    Minitest::Reporters.use! [Minitest::Reporters::DefaultReporter.new, JSONReporter.new]
  end

  require 'minitest/autorun'
end

require_relative "../lib/application"

# Creates a fake random uuid
def uuid()
  # Random UUID4
  SecureRandom.uuid
end

# Creates a normal SHA256 multihash
def multihash()
  require 'multihashes'
  require 'digest'
  require 'base58'

  digest = Digest::SHA256.digest(uuid())
  multihash_binary_string = Multihashes.encode digest, 'sha2-256'
  Base58.binary_to_base58(multihash_binary_string, :bitcoin)
end

# Creates a fake random revision
def revision()
  # Random SHA1
  SecureRandom.hex(40)
end

class WarningIO < StringIO
  IGNORE_WARNINGS = [
    /useless use/,
    /gems\/websocket-extensions-0.1.3.+warning:/,
    /\/lib\/puma.+warning:/,
  ]

  def initialize(stderr)
    super("")
    @stderr = stderr
  end

  def puts(s)
    if !s.is_a?(String) || ENV['VERBOSE'] || IGNORE_WARNINGS.none?{ |pattern| pattern.match(s) }
      @stderr.puts(s)
    end
  end

  def write(s)
    if !s.is_a?(String) || ENV['VERBOSE'] || IGNORE_WARNINGS.none?{ |pattern| pattern.match(s) }
      @stderr.write(s)
    end
  end
end

# Add a warning ignorer
old_stderr = $stderr
$stderr = WarningIO.new(old_stderr)

class MiniTest::Test
  remove_method :setup if respond_to? :setup
  def setup
  end

  alias_method :setup_base, :setup

  remove_method :teardown if respond_to? :teardown
  def teardown
  end

  alias_method :teardown_base, :teardown
end
