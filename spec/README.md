# Testing

To test the web client, there are several layers. The first is the unit testing, which isolates individual classes and their methods and tests each for their specific behaviors. This is done using the [MiniTest](http://docs.seattlerb.org/minitest/) framework. The next are acceptance tests, which look at broader feature sets and ensures actions are possible through emulating the actions directly upon the website in a browser. These use [Capybara](http://teamcapybara.github.io/capybara/). The last are the javascript tests, which are just unit tests for client-side code. For this, we make use of [Jasmine](https://jasmine.github.io/), which runs the tests within the javascript environment of a native web browser.

To run all of the unit and acceptance tests, invoke the following:

```
rake test
```

This will run each of the three levels of testing and show the progress as it completes. It is possible to only test parts of the system at a time. To see a list of possible actions, type:

```
rake --tasks
```

The following are useful:

```
# Test just the models
rake test:model

# Test just the Occam::Account model
rake test:model[account]

# Only acceptance tests
rake test:acceptance

# Filter acceptance tests (note quotes)
rake "test:acceptance[bookmark_management,Bookmark an object while logged in]"
```

## Output Modes

By default, the test output will be a stream of dots. Each dot corresponds to a test within
the test suite. A '.' is the good outcome as it means the test passed. An 'F' means the test
failed, as in the assertion it was looking for was invalid. An 'E' means the interpreter
itself did not like the code. This case means the code itself is invalid and cannot be run.

To produce other forms of output for the tests, you can use the `OUTPUT` variable. For
instance, you can list the full names of the tests and list them as a hierarchical list
which indents them underneath their respective modules. To do that, try this:

```
OUTPUT=SPEC rake test
```

Where, once again, all of the subsets, such as `test:model` and the like, are also valid.

## Test Seeds and Determinism

Tests are executed in a random order to ensure that any state leakage may be caught over time.
You will notice a seed printed out at the beginning of the test output.
This will generally be unique each time you run the tests.

If you find a non-deterministic error reflected in the tests, that is an error that goes
away or differs in some way from one test run to another, you may take note of the seed.

To run a test suite with a known seed, you can execute the tests with the following:

```
SEED=1234 rake test
```

And you should generally use the same command that you ran before. If you were only running
the unit tests, you'd use:

```
SEED=1234 rake test:model
```

Where the `SEED` variable is set to the reported seed, which is `1234` here. You'll
see that seed being reported once more in the test report instead of a new seed. It
is good to check that, and all of the so-called obvious things, whenever you are
facing the effects of randomness.

## File Layout

```
/spec/.occam         - [generated] Testing Occam node, rebuilt with every test run.
/spec/helper.rb      - Base file all tests use that sets up the testing environment
/spec/README.md      - This file
/spec/tests.json     - [generated] JSON test output (via `rake test OUTPUT=JSON`)
/spec/web-config.yml - Occam Configuration for the test run
/spec/models         - Classes that wrap data structures used in Occam.
/spec/acceptance     - Tests for features via navigating the website.
```

## Debugging CI and Selenium

Sometimes you may get different behavior on GitLab or other CI environments when
using Selenium. The CI uses the `selenium/standalone-chrome` docker image to run
its tests. By default, the Selenium server is installed locally. We can also use
that same docker image ourselves.

First, we want to spin up that Selenium docker image:

```shell
docker run --rm --net host selenium/standalone-chrome
```

And then pass along the information to the test harness:

```shell
SELENIUM_URL=http://localhost:4444/wd/hub rake test
```

This will invoke the testing system using that already running dockerized
Selenium and recreate the behavior seen on the CI somewhat better.
