require_relative "helper"

feature "Creating Workset and Experiment", :js => true do
  scenario "Creating a fresh workset and a fresh experiment" do
    raise Minitest::Skip, "UI changes break this test", caller

    username = "wilkie-#{uuid()}"
    password = "foobar-#{uuid()}"

    account = Occam::Account.create(username, password)
    person  = account.person

    visit '/login'

    fill_in 'username', :with => username
    fill_in 'password', :with => password

    click_button "login"

    # We should be on the login page
    assert current_path == "/people/#{person.identity.uri}",
      "We failed to login"

    # Go to the collection
    find("ul.tabs > li.tab > a[href=\"/people/#{person.identity.uri}/collection\"]").click

    # Go to the object creation form
    find("a[href=\"/new\"]").click

    # Set type to workset
    fill_in "new-object-type", :with => "workset"

    # Create the workset
    fill_in "new-object-name", :with => "new workset"
    click_button :name => "add"

    # We should be on the workset page
    objectName = page.find('#object-name a').text
    assert objectName == "new workset",
      "Workset has the wrong name (or not on the Workset page)"
    objectType = page.find('#object-type').text
    assert objectType == "workset",
      "Workset has the wrong type"

    worksetPage = current_url

    find("#add-new-object").click

    # Now create an experiment (the default)
    fill_in "new-object-name", :with => "new experiment"
    click_button :name => "add"

    # Wait for it to navigate away from the workset page, if it does
    page.document.synchronize do
      raise Capybara::ElementNotFound if current_url == worksetPage
    end

    # We should be on the experiment page
    objectName = page.find('#object-name a').text
    assert objectName == "new experiment",
      "Experiment has the wrong name (or not on the Experiment page)"
    objectType = page.find('#object-type').text
    assert objectType == "experiment",
      "Experiment has the wrong type"
  end
end
