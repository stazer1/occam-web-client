require_relative "helper"


feature "markdown_editing", :js => true do
  # You are are on the Object page
  background do
    @object = Occam::Object.create(:name => "foo", :type => "bar", :account => admin)
    @object.setPermission("read", "true")
    @object.setPermission("write", "true")
  end

  scenario "Edit the description of an object we have write access to" do
    login_as_user

    visit @object.url

    # Go to the Details page
    find("ul.tabs > li.tab > a[href=\"#{@object.url(:path => "metadata")}\"]").click

    # Click on the edit link
    click_link('edit-markdown')

    # Fill out some markdown
    page.fill_in 'edit-object-description', :with => "testDescriptionData"

    click_button('edit-object-submit')

    assert page.has_content?("testDescriptionData"),
      "Updated object description not present"
  end
end
