require_relative "helper"

feature "Uploading Files", :js => true do
  # You are are on the Object page
  background do
    # We need write access to the object
    @object = Occam::Object.create(:name => "foo", :type => "bar", :account => admin)
    @object.setPermission("read", "true")
    @object.setPermission("write", "true")
  end

  scenario "Drag and drop file to an object we have write access to" do
    if ENV['GITLAB_CI'] == "true"
      raise Minitest::Skip, "Breaks on GitLab-CI", caller
    end

    login_as_user

    # Go to the object page
    visit @object.url(:path => "files")

    # Open the upload bar
    find(".new-actions-bar button.show-upload-file.bound").click

    # Get a file, and send it to the browser
    testFilePath = File.join(File.dirname(__FILE__), "data", "occam_test_file.txt")
    attach_file("fileToUpload", testFilePath)

    # Click "UPLOAD!"
    click_button 'objectFileUpload'

    # Check to see that the file is in the list
    assert page.has_content?("occam_test_file.txt"),
      "File was not successfully uploaded."
  end
end
