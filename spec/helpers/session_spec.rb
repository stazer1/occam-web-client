require_relative "helper"

describe Occam::Controller::Helpers::SessionHelpers, :type => :helper do
  class HelperTest < HelperContext
    include Occam::Controller::Helpers::SessionHelpers
  end

  before do
    @app = HelperTest.new
  end

  describe "current_account" do
    it "should retrieve nil when there is no valid Account in the session" do
      @app.current_account.must_be_nil
    end

    it "should retrieve nil when the session data is invalid" do
      app = HelperTest.new(:session => {:token     => "bogus",
                                        :identity  => "blah"})
      app.current_account.must_be_nil
    end

    it "should retrieve an Occam::Account when the session data is valid" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      app = HelperTest.new(:session => {:token     => account.token,
                                        :identity  => account.person.identity.uri})
      app.current_account.must_be_instance_of Occam::Account
    end

    it "should retrieve an Occam::Account based on the token from the X-Occam-Token HTTP Header" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      token = {:token => account.token,
               :identity => account.person.identity.uri}
      encrypted_token = "SECRET_TOKEN"
      app = HelperTest.new(:env => {"HTTP_X_OCCAM_TOKEN" => encrypted_token})
      app.stubs(:decrypt_token).with(anything).returns(token)
      app.current_account.must_be_instance_of Occam::Account
    end

    it "should return nil for an invalid token from the X-Occam-Token HTTP Header" do
      new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      token = {:token => "bogus",
               :identity => "blah"}
      encrypted_token = "SECRET_TOKEN"
      app = HelperTest.new(:env => {"HTTP_X_OCCAM_TOKEN" => encrypted_token})
      app.stubs(:decrypt_token).with(anything).returns(token)
      app.current_account.must_be_nil
    end

    it "should decrypt the token from the X-Occam-Token HTTP Header" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      token = {:token => account.token,
               :identity => account.person.identity.uri}
      encrypted_token = "SECRET_TOKEN"
      app = HelperTest.new(:env => {"HTTP_X_OCCAM_TOKEN" => encrypted_token})
      app.stubs(:decrypt_token).with(encrypted_token).returns(token)
      app.current_account.token == token[:token]
    end
  end

  describe "current_person" do
    it "should retrieve nil when there is no valid Account in the session" do
      @app.current_person.must_be_nil
    end

    it "should retrieve nil when the session data is invalid" do
      app = HelperTest.new(:session => {:token    => "bogus",
                                        :identity => "blah"})
      app.current_person.must_be_nil
    end

    it "should retrieve an Occam::Person when the session data is valid" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      app = HelperTest.new(:session => {:token    => account.token,
                                        :identity => account.person.identity.uri})
      app.current_person.must_be_instance_of Occam::Person
    end

    it "should retrieve an Occam::Person based on the token from the X-Occam-Token HTTP Header" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      token = {:token => account.token,
               :identity => account.person.identity.uri}
      encrypted_token = "SECRET_TOKEN"
      app = HelperTest.new(:env => {"HTTP_X_OCCAM_TOKEN" => encrypted_token})
      app.stubs(:decrypt_token).with(anything).returns(token)
      app.current_person.must_be_instance_of Occam::Person
    end

    it "should return nil for an invalid token from the X-Occam-Token HTTP Header" do
      new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      token = {:token => "bogus",
               :identity => "weasel"}
      encrypted_token = "SECRET_TOKEN"
      app = HelperTest.new(:env => {"HTTP_X_OCCAM_TOKEN" => encrypted_token})
      app.stubs(:decrypt_token).with(anything).returns(token)
      app.current_person.must_be_nil
    end
  end

  describe "logged_in?" do
    it "should return false when there is no valid Account in the session" do
      @app.logged_in?.must_equal false
    end

    it "should return false when the session data is invalid" do
      app = HelperTest.new(:session => {:token     => "bogus",
                                        :identity  => "identity"})
      app.logged_in?.must_equal false
    end

    it "should return true when the session data is valid" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      app = HelperTest.new(:session => {:token     => account.token,
                                        :identity  => account.person.identity.uri})
      app.logged_in?.must_equal true
    end

    it "should return true when the token from the X-Occam-Token HTTP Header is valid" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      token = {:token => account.token,
               :identity => account.person.identity.uri}
      encrypted_token = "SECRET_TOKEN"
      app = HelperTest.new(:env => {"HTTP_X_OCCAM_TOKEN" => encrypted_token})
      app.stubs(:decrypt_token).with(anything).returns(token)
      app.logged_in?.must_equal true
    end

    it "should return false for an invalid token from the X-Occam-Token HTTP Header" do
      new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      token = {:token => "bogus",
               :identity => "weasel"}
      encrypted_token = "SECRET_TOKEN"
      app = HelperTest.new(:env => {"HTTP_X_OCCAM_TOKEN" => encrypted_token})
      app.stubs(:decrypt_token).with(anything).returns(token)
      app.logged_in?.must_equal false
    end
  end

  describe "logout" do
    it "should set the session token to nil when it was already nil" do
      app = HelperTest.new()
      app.logout

      app.session[:token].must_be_nil
    end

    it "should set the session token to nil when it was not nil" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])
      token = {:token => account.token,
               :identity => account.person.identity.uri}
      encrypted_token = "SECRET_TOKEN"
      app = HelperTest.new(:env => {"HTTP_X_OCCAM_TOKEN" => encrypted_token})
      app.stubs(:decrypt_token).with(anything).returns(token)

      app.current_account

      app.logout

      app.session[:token].must_be_nil
    end
  end

  describe "login" do
    it "should raise Occam::Daemon::Error when the username and password are invalid" do
      new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])

      app = HelperTest.new()
      assert_raises Occam::Daemon::Error do
        app.login("bogus", "weasel")
      end
    end

    it "should raise Occam::Daemon::Error when the username is invalid" do
      new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])

      app = HelperTest.new()
      assert_raises Occam::Daemon::Error do
        app.login("bogus", "foobar")
      end
    end

    it "should raise Occam::Daemon::Error when the password is invalid" do
      new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])

      app = HelperTest.new()
      assert_raises Occam::Daemon::Error do
        app.login("wilkie", "weasel")
      end
    end

    it "should return an Occam::Account when the username and password is valid" do
      new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])

      app = HelperTest.new()
      app.login("wilkie", "foobar").must_be_instance_of Occam::Account
    end

    it "should set the session token when the username and password is valid" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])

      app = HelperTest.new()
      app.login("wilkie", "foobar")
      app.session[:token].must_equal account.token
    end

    it "should set the session roles when the username and password is valid" do
      new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])

      app = HelperTest.new()
      app.login("wilkie", "foobar")
      app.session[:roles].must_equal ["foo"]
    end

    it "should set the session identity when the username and password is valid" do
      account = new_account(:username => "wilkie", :password => "foobar", :roles => ["foo"])

      app = HelperTest.new()
      app.login("wilkie", "foobar")
      app.session[:identity].must_equal account.person.identity.uri
    end
  end
end
