"use strict";

import Helper from '../helper';

import Gallery from '../../../public/js/occam/gallery';

describe('Gallery', () => {
    beforeEach(function() {
        jasmine.addMatchers(DOMCustomMatchers);
        jasmine.addMatchers(Helper.VisibilityMatchers);

        this.gallery = document.createElement('div');
        this.gallery.classList.add('gallery');

        let preview = document.createElement("ul");
        preview.classList.add('preview');

        let previewItem = document.createElement("li");
        previewItem.setAttribute('id', 'gallery-' + Helper.randomString() + '-0');
        preview.appendChild(previewItem);
        this.gallery.appendChild(preview);

        let thumbs = document.createElement("ul");
        thumbs.classList.add("thumbs");
        this.gallery.appendChild(thumbs);

        document.body.appendChild(this.gallery);
    });

    describe('.constructor', () => {
        it('should throw TypeError when element is not passed', function() {
            expect( () => new Gallery() ).toThrowError(TypeError);
        });

        it('should throw TypeError when element is not HTMLElement', function() {
            expect( () => new Gallery(".gallery") ).toThrowError(TypeError);
        });

        it('should assign data-loaded-index attribute', function() {
            let selector = new Gallery(this.gallery);
            expect(selector.element).toHaveAttribute('data-loaded-index');
        });
    });

    describe('.load', () => {
        it('should throw TypeError when element is not passed', function() {
            expect( () => Gallery.load() ).toThrowError(TypeError);
        });

        it('should throw TypeError when element is not HTMLElement', function() {
            expect( () => Gallery.load(".gallery") ).toThrowError(TypeError);
        });
    });
});
