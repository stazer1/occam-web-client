# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Task API
class Occam
  module Controllers
    # This module represents the routes for querying services.
    class ServiceController < Occam::Controller
      options '/services' do
        handleCORS('*')
        status 200
      end

      # Retrieves a listing of services based on the offered criteria
      get '/services' do
        # The query
        query = params["search"] || ""

        # Report JSON
        content_type "application/json"

        # Get the listing
        ret = Service.all(:json => true, :query => query)

        # Append svg icon IDs
        ret[:services].each do |serviceInfo|
          serviceInfo[:iconID] = Occam::Object.serviceIconIDFor(serviceInfo[:service])
          serviceInfo[:iconGroup] = "services"
        end

        ret.to_json
      end
    end
  end

  use Controllers::ServiceController
end
# @!endgroup
