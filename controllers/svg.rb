# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'objects'

# @api api
# @!group SVG Routes
class Occam
  module Controllers
    class SVGController < Occam::Controller
      # Allow dynamic colors for any arbitrary svg files
      # TODO: shouldn't assume <?xml ... ?> is the first line
      def recolor_svg()
        content_type "image/svg+xml"
        forever_cache

        params["name"] = params["splat"].join

        if params["hex"]
          params["color"] = "##{params["hex"]}"
        elsif params["hue"] and params["sat"] and params["light"]
          params["color"] = "hsl(#{params["hue"]}, #{params["sat"]}%, #{params["light"]}%)"
        end

        if params["color"]
          require 'base64'

          headers 'Content-Type' => "image/svg+xml"
          css = "path, rect, ellipse, polygon, circle { fill: #{params["color"]} !important; stroke: transparent !important }"
          embed = Base64.encode64(css)
          extra = "<?xml-stylesheet type=\"text/css\" href=\"data:text/css;charset=utf-8;base64,#{embed}\" ?>"

          size = File.size("images/#{params[:name]}.svg") + extra.length

          headers["Content-Length"] = size

          stream do |out|
            File.open("images/#{params[:name]}.svg") do |f|
              out << f.readline
              out << extra
              out << f.read
            end
          end
        else
          send_file "public/images/#{params[:name]}.svg"
        end
      end

      # Retrieves an icon for a particular type of object in the given color.
      get "/images/dynamic/hue/:hue/sat/:sat/light/:light/icons/for" do
        params["object-type"] ||= "object"
        redirect Occam::Object.iconURLFor(params["object-type"],
                                          :hue => params[:hue],
                                          :sat => params[:sat],
                                          :light => params[:light])
      end

      get "/images/dynamic/hex/:hex/icons/for" do
        params["object-type"] ||= "object"
        redirect Occam::Object.iconURLFor(params["object-type"],
                                          :hex => params[:hex])
      end

      get "/images/dynamic/color/:color/icons/for" do
        params["object-type"] ||= "object"
        redirect Occam::Object.iconURLFor(params["object-type"],
                                          :color => params[:color])
      end

      get "/images/icons/for" do
        params["object-type"] ||= "object"
        redirect Occam::Object.iconURLFor(params["object-type"])
      end

      get "/images/dynamic/hex/:hex/*.svg" do
        recolor_svg()
      end

      get "/images/dynamic/color/:color/*.svg" do
        recolor_svg()
      end

      get "/images/dynamic/hue/:hue/sat/:sat/light/:light/*.svg" do
        recolor_svg()
      end

      get "/images/dynamic/hex/:hex/*.svg" do
        recolor_svg()
      end

      get "/images/dynamic/color/:color/*.svg" do
        recolor_svg()
      end
    end
  end

  use Controllers::SVGController
end
# @!endgroup
