# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
require 'rss'

# @api api
# @!group Object API
class Occam
  module Controllers
    class ObjectController < Occam::Controller
      # @!macro object_route
      #   @param [String] id The id of the object.
      #   @param [String] revision The (optional) revision of the object. When no
      #                            revision is given, it uses the one marked by the
      #                            object as 'latest'.

      # A regular expression that captures an Object ID with a valid revision hash.
      OBJECT_ROUTE_REGEX = %r{(/objects)?/(?<id>[0-9a-zA-Z]{46})(/((?<revision>[0-9a-zA-Z]{30,46})|(?<link>:[0-9a-zA-Z-]+))(?<index>([/]\d+)+)?)?}

      # @api controller-helpers
      # Resolves the object from the route
      def resolveObject(as = Occam::Object)
        require 'cgi'

        tokens = []

        # Decrypt any file access tokens passed normally
        if params["token"]
          dummy = Occam::Object.new(:id => params[:id],
                                    :revision => params[:revision],
                                    :info => {})
          token = decrypt_token(params["token"], dummy)
          if token && token[:token]
            tokens.append(token[:token])
          else
            # XXX: Is this bad?? This gets appended to things (with a '-T', but still)
            tokens.append(params["token"])
          end
        end

        if params[:link] && params[:link].start_with?(":")
          params[:link] = params[:link][1..-1]
        end

        # Look to our Object Cache when possible
        @@cache = Occam::ObjectCache.new() unless defined?(@@cache)
        @object = @@cache.resolve({
          :id => params[:id],
          :revision => params[:revision],
          :path => params[:path],
          :index => ((params[:index] || "").split("/")[1..-1] || []).map(&:to_i),
          :link => params[:link],
          :tokens => tokens,
          :account => current_account,
          :remote => params.has_key?("discover"),
        }, as)

        if not @object.exists?
          handleCORS('null')
          status 404
          requestedObject = @object
          @object = nil
          halt 404, render(:slim, :"objects/404", :locals => {
            :requestedObject => requestedObject
          })
          return nil
        end

        # Do not cache, if staged
        if @object.link
          cache_control :no_cache
        end

        if @object.resource?
          @object = @object.as(Occam::Resource)
        end

        @object
      end

      # @api controller-helpers
      # Renders a particular object tab or view.
      def renderObjectView(tab=nil, options={})
        params[:tab] = tab

        if tab && (current_layout.view == :minimal || request.xhr? && !params[:full] && !params[:modal])
          render :slim, :"objects/_#{tab}", :layout => current_layout.layout, :locals => {
            :object => @object,
          }.update(options)
        else
          render :slim, :"objects/show", :layout => !request.xhr?, :locals => {
            :object => @object,
          }.update(options)
        end
      end

      # Retrieve an Object creation form.
      get "/new" do
        render :slim, :"objects/new", :layout => !request.xhr?, :locals => {
          :errors => []
        }
      end

      # Retrieve the object status (explicitly)
      get %r{#{OBJECT_ROUTE_REGEX}/status/?} do
        @object = resolveObject()
        content_type "application/json"
        @object.status.to_json
      end

      # Retrieve icon for the object
      get %r{#{OBJECT_ROUTE_REGEX}/icon/?} do
        @object = resolveObject()
        redirect @object.iconURL
      end

      # Retrieve form to add a new object to the given object
      get %r{#{OBJECT_ROUTE_REGEX}/objects/new/?} do
        @object = resolveObject()
        render :slim, :"objects/add", :layout => !request.xhr?, :locals => {
          :object => @object,
          :errors => []
        }
      end

      # This route creates or links an object into the given object "contains"
      post %r{#{OBJECT_ROUTE_REGEX}/objects/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        new_object = @object.create(params)

        case format
        when 'application/json'
          content_type "application/json"
          {
            :url => new_object.url,
            :object => new_object.info.update({:id => new_object.id, :uid => new_object.uid, :revision => new_object.revision})
          }.to_json
        else
          redirect new_object.url
        end
      rescue Occam::Daemon::Error => e
        status 422

        case format
        when 'application/json'
          {
            'errors': [e.to_s]
          }.to_json
        else
          render :slim, :"objects/add", :layout => !request.xhr?, :locals => {
            :object => @object,
            :errors => [e]
          }
        end
      end

      # This route creates or links an object into the given workflow
      post %r{#{OBJECT_ROUTE_REGEX}/workflow/?} do
        @object = resolveObject(:as => Occam::Workflow)
        params[:type]="configuration"
        params[:name]="temporary_name"
        #new_object = object.create(params)

        #redirect new_object.url
      end

      # Renders the input for an object
      get %r{#{OBJECT_ROUTE_REGEX}/inputs/?((?<input_index>\d+)/?)?} do
        @object = resolveObject()
        # Allow all AJAX requests for this route
        handleCORS('*')

        # Determine the input index to render
        index = params[:input_index].to_i

        # Get the node index, if there is one
        nodeIndex = (params["node_index"] || 0).to_i

        inputs = @object.info[:inputs] || []

        if 0 > index || inputs.length <= index
          status 404
          return
        end

        input = inputs[index]

        if input[:type] == "configuration" and !input[:schema].nil?
          # This is a configuration
          # We can render a form for this

          # First, retrieve the schema (if it is a file)
          schema = input[:schema]
          if !schema.is_a?(Hash)
            schema = @object.retrieveJSON(input[:schema])
          end

          # Use this to render the form
          render :slim, :"objects/_configuration", :layout => !request.xhr?, :locals => {
            :reviewing           => @params[:params] && @params[:params].has_key?("review"),
            :object              => @object,
            :revision            => @object.revision,
            :schema              => schema,
            :configuration_index => index,
            :index               => nodeIndex,
            :defaults            => {}
          }
        else
          # This is a normal input
          # Render something else then
          ""
        end
      end

      # Retrieves the configurations of a node in a workflow
      get %r{#{OBJECT_ROUTE_REGEX}/configure-node/(?<node_index>\d+)/?} do
        @object = resolveObject()

        # Get the workflow
        workflow = resolveObject(Occam::Workflow)

        params[:node_index] = params[:node_index].to_i

        render :slim, :"workflows/_configuration_pane", :layout => !request.xhr?, :locals => {
          :workflow => workflow,
          :index    => params[:node_index],
          :connection => workflow.connectionAt(params[:node_index])
        }
      end

      # Retrieves the configuration form for a node and input in the workflow.
      get %r{#{OBJECT_ROUTE_REGEX}/configure-node/(?<node_index>\d+)/(?<input_index>\d+)/?} do
        @object = resolveObject()

        # Get the workflow
        workflow = resolveObject(Occam::Workflow)

        params[:node_index] = params[:node_index].to_i

        # Determine the input index to render
        index = params[:input_index].to_i

        # Get input info for that node
        connection = workflow.connectionAt(params[:node_index])
        input = connection[:inputs][index]

        # Bail if this input is not a configuration
        if input.nil? || input[:type] != "configuration"
          status 406
          return
        end

        # The data for the existing configuration is blank, to start
        data = {}

        # Get configuration object, if it exists
        configuration_object = workflow.inputsAt(params[:node_index], index)[0]
        if configuration_object
          configuration_object = workflow.resolveNode(configuration_object)
          configuration_object = configuration_object.as(Occam::Configuration)
          configuration_object.info(true)

          # Get the existing configuration data
          data = configuration_object.data
        else
          # We are creating a new configuration, perhaps
          # We need that schema into input[:schema], then
          # Resolve the input object
          object = workflow.resolveNode(connection)

          # Get the configuration input
          configurationInfo = (object.info(true)[:inputs] || [])[index]
          if configurationInfo
            # Get the schema
            input[:schema] = configurationInfo[:schema]
            if !input[:schema].is_a?(Hash)
              input[:schema] = object.retrieveJSON(input[:schema])
            end
          end
        end

        # First, retrieve the schema (if it is a file)
        schema = input[:schema]
        if !schema.is_a?(Hash)
          schema = configuration_object.schema
        end

        # Use this to render the form
        render :slim, :"objects/configure", :layout => !request.xhr?, :locals => {
          :reviewing           => @params[:params] && @params[:params].has_key?("review"),
          :object              => @object,
          :revision            => @object.revision,
          :schema              => schema,
          :configuration_index => index,
          :name                => input[:name] || "Configure",
          :editing             => false,
          :index               => 0,
          :defaults            => data,
          :errors              => []
        }
      end

      # Retrieves the configuration form of an object
      get %r{#{OBJECT_ROUTE_REGEX}/configure/(?<input_index>\d+)/?} do
        @object = resolveObject()

        # Determine the input index to render
        index = params[:input_index].to_i

        # Get the node index, if there is one
        nodeIndex = (params["node_index"] || 0).to_i

        inputs = @object.info[:inputs] || []

        if 0 > index || inputs.length <= index
          status 404
          return
        end

        input = inputs[index]

        # The index must point to a configuration input
        if input[:type] != "configuration"
          status 406
          return
        end

        # There needs to be a URL to save the schema
        url = nil

        # First, retrieve the schema (if it is a file)
        schema = input[:schema]
        if !schema.is_a?(Hash)
          schema = @object.retrieveJSON(input[:schema])

          # And then get the URL to update the schema
          url = @object.url(:path => "files/#{input[:schema]}")
        end

        # Use this to render the form
        render :slim, :"objects/configure", :layout => !request.xhr?, :locals => {
          :reviewing           => @params[:params] && @params[:params].has_key?("review"),
          :object              => @object,
          :revision            => @object.revision,
          :schema              => schema,
          :configuration_index => index,
          :name                => input[:name] || "Configure",
          :editing             => true,
          :url                 => url,
          :index               => 0,
          :defaults            => {},
          :errors              => []
        }
      end

      # Retrieves the file configuration of a workflow node
      get %r{#{OBJECT_ROUTE_REGEX}/configure-node/(?<node_index>\d+)/file/?} do
        @object = resolveObject()

        # Get the workflow
        workflow = resolveObject(Occam::Workflow)

        # Get the node index, if there is one
        nodeIndex = (params["node_index"] || 0).to_i

        # Resolve the object at that index
        objectInfo = workflow.data[:connections][nodeIndex]
        object = Occam::Object.new(:id => objectInfo[:id],
                                   :revision => objectInfo[:revision],
                                   :account => current_account)

        # Get the file selection and flags
        data = workflow.data[:connections][nodeIndex][:file]

        render :slim, :"objects/_configuration-file", :layout => !request.xhr?, :locals => {
          :reviewing           => @params[:params] && @params[:params].has_key?("review"),
          :object              => object,
          :data                => data
        }
      end

      # Retrieves the target configuration of a workflow node
      get %r{#{OBJECT_ROUTE_REGEX}/configure-node/(?<node_index>\d+)/target/?} do
        @object = resolveObject()

        # Get the workflow
        workflow = resolveObject(Occam::Workflow)

        # Get the node index, if there is one
        nodeIndex = (params["node_index"] || 0).to_i

        schema = Occam::Workflow.targetConfigurationSchema
        data = workflow.data[:connections][nodeIndex][:deploy]

        render :slim, :"objects/_configuration", :layout => !request.xhr?, :locals => {
          :reviewing           => @params[:params] && @params[:params].has_key?("review"),
          :object              => @object,
          :revision            => @object.revision,
          :schema              => schema,
          :configuration_index => 0,
          :index               => 0,
          :data                => data
        }
      end

      # Updates a configuration of a workflow object
      post %r{#{OBJECT_ROUTE_REGEX}/configure-node/(?<node_index>\d+)/(?<wire_index>\d+)/?} do
        @object = resolveObject()

        # Get the workflow
        workflow = resolveObject(Occam::Workflow)

        # Get the wire index so we know which configuration this is
        # If there is an existing configuration (And this workflow owns it)
        # We will update it
        nodeIndex = params[:node_index].to_i
        wireIndex = params[:wire_index].to_i

        inputs = workflow.inputsAt(nodeIndex, wireIndex)

        # If there is no object connected, update the workflow to include an empty configuration
        if inputs.empty?
          # Add a blank configuration
          input = workflow.createConfigurationFor(nodeIndex, wireIndex)
          input.info(true)
          workflow = input.parent.as(Occam::Workflow)
          workflow.info(true)
        else
          input = workflow.resolveNode(inputs[0])
        end

        input = input.as(Occam::Configuration)
        input.info(true)

        # Decode the configuration form data sent from the browser
        params[:data] = Occam::Workflow.decode_base64(params[:data] || {})
        params[:nullify] = Occam::Workflow.decode_base64(params[:nullify] || {})
        params[:datatype] = Occam::Workflow.decode_base64(params[:datatype] || {})

        # Normal updating of JSON data
        items = ObjectInfo.parseParams(params)

        # Submit the new configuration data to the configuration object
        input = input.configure(items, :type => "json")

        # Commit a new version of the configuration
        input = input.commit
        input.info(true)

        if input.parent
          workflow = input.parent.as(Occam::Workflow)
          workflow.info(true)
        end

        inputs = workflow.inputsAt(nodeIndex, wireIndex)
        if inputs.empty?
          # Update workflow data to add this to the structure (as a hidden node)
          inputIndex = workflow.append(input, :visibility => :hidden)
          workflow.connect(inputIndex, -1, nodeIndex, wireIndex, :visibility => :hidden)
          inputs = [workflow.connectionAt(inputIndex)]
        end

        # Update configuration node in workflow
        inputs[0][:revision] = input.revision

        # Return to the new version of the object
        workflow = workflow.save
        workflow.info(true)
        workflow.data.to_json

        # Commit a new version of the workflow
        workflow = workflow.commit

        if request.xhr?
        else
          redirect workflow.parent.url
        end
      end

      # Removes an author
      delete %r{#{OBJECT_ROUTE_REGEX}/authorships/(?<person_id>[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12})/?} do
        @object = resolveObject()
        role     = params["role"]
        objectId = params[:person_id]
        username = params["username"]

        record = Occam::Authorship.new(:role    => role,
                                       :object  => @object,
                                       :name    => username,
                                       :person  => Occam::Person.new(:id => objectId),
                                       :account => current_account)

        record.destroy!

        if request.referrer
          redirect request.referrer
        else
          redirect @object.url
        end
      end

      # Adds an author
      post %r{#{OBJECT_ROUTE_REGEX}/authorships/?} do
        @object = resolveObject()
        role     = params["role"]
        objectId = params["object-id"]
        username = params["username"]

        Occam::Authorship.create(:role    => role,
                                 :object  => @object,
                                 :name    => username,
                                 :person  => Occam::Person.new(:id => objectId),
                                 :account => current_account)

        if request.referrer
          redirect request.referrer
        else
          redirect @object.url
        end
      end

      # Creates a trust association
      post %r{#{OBJECT_ROUTE_REGEX}/trust/?} do
        @object = resolveObject()
        @object.trust!

        if request.xhr?
        else
          if request.referrer
            redirect request.referrer
          else
            redirect @object.url
          end
        end
      end

      # Removes a trust association
      delete %r{#{OBJECT_ROUTE_REGEX}/trust/?} do
        @object = resolveObject()
        @object.untrust!

        if request.referrer
          redirect request.referrer
        else
          redirect @object.url
        end
      end

      # Close a staged object that is being edited
      post %r{#{OBJECT_ROUTE_REGEX}/close/?} do
        @object = resolveObject()

        # Remove it from 'active'
        link = Occam::Link.new(:source => current_person.identity.uri,
                               :target => @object,
                               :relationship => "active")
        current_person.destroyLink!(link)

        # Delete the local link
        @object.untrack!

        # Return to the active page
        redirect "/people/#{current_person.identity.uri}/active"
      end

      # Open an owned object for editing
      post %r{#{OBJECT_ROUTE_REGEX}/edit/?} do
        @object = resolveObject()

        # Link to the object
        link_id = @object.track()
        current_person.createLink(:relationship => "active",
                                  :object       => @object,
                                  :account      => current_account,
                                  :tracked      => link_id)
        newObject = @object.as(Occam::Object, :link => link_id)

        url = newObject.url

        # Return the new object information if that is requested, otherwise redirect to the new object
        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])
        case format
        when 'application/json'
          content_type "application/json"

          # When json is requested, this is considered an API call and we will return
          # the command's output
          {
            :object => {
              :id => newObject.withinUuid,
              :revision => newObject.withinRevision,
              :index => newObject.index,
              :link => newObject.link
            },
            :url => url
          }.to_json
        else
          # When html is requested (common case)
          # We will redirect to the html content (the object itself)
          redirect url
        end
      end

      # Fork an object to the given object
      post %r{#{OBJECT_ROUTE_REGEX}/fork/?} do
        @object = resolveObject()

        # Determine index
        params["to"]["index"] = params["to"]["index"].split("/").map(&:to_i)

        # Symbolize the keys so we can pass them to Occam::Object.new
        options = symbolize_keys(params["to"])
        if options[:link] == ""
          options.delete(:link)
        end

        options[:account] = current_account

        # Get the object we are cloning 'to'
        to = nil
        if options[:id] && options[:id] != ""
          to = Occam::Object.new(options)
        end

        if to.nil? || (current_person && current_person.id == to.id)
          to = nil
        end

        # Clone
        newObject = @object.clone(to, :name => params["name"])
        url = newObject.url

        link_id = nil

        if to.nil?
          # Link to the object
          link_id = newObject.track()
          current_person.createLink(:relationship => "active",
                                    :object       => newObject,
                                    :account      => current_account,
                                    :tracked      => link_id)
          newObject = newObject.as(Occam::Object, :link => link_id)

          url = newObject.url
        end


        # Return the new object information if that is requested, otherwise redirect to the new object
        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])
        case format
        when 'application/json'
          content_type "application/json"

          # When json is requested, this is considered an API call and we will return
          # the command's output
          {
            :object => {
              :id => newObject.withinUuid,
              :revision => newObject.withinRevision,
              :index => newObject.index,
              :link => newObject.link
            },
            :url => url
          }.to_json
        else
          # When html is requested (common case)
          # We will redirect to the html content (the object itself)
          redirect url
        end
      end

      # Retrieve form to fork the object
      get %r{#{OBJECT_ROUTE_REGEX}/fork/?} do
        @object = resolveObject()
        render :slim, :"objects/fork", :layout => !request.xhr?, :locals => {
          :object => @object,
          :errors => []
        }
      end

      # Retrieve queued workflow runs
      get %r{#{OBJECT_ROUTE_REGEX}/runs/?} do
        @object = resolveObject()
        # Pull out the object (as a workflow)
        workflow = resolveObject(Occam::Workflow)

        content_type "application/json"
        workflow.runs.to_json
      end

      # Get the list of versions
      get %r{#{OBJECT_ROUTE_REGEX}/versions/?} do
        @object = resolveObject()
        content_type "application/json"

        @object.versions(:json => true).to_json
      end

      # Cancel the job
      post %r{#{OBJECT_ROUTE_REGEX}/jobs/(?<job_index>\d+)/(?<action>\w+)/?} do
        @object = resolveObject()
        if (params[:action] == "cancel")

          job = Occam::Job.new(:id      => params[:job_index],
                               :account => @object.account)

          job.cancel
        end

        if request.xhr?
          content_type "application/json"
          job.info.to_json
        elsif request.referrer
          redirect request.referrer
        end
      end

      # Retrieve the job task instantiation
      get %r{#{OBJECT_ROUTE_REGEX}/jobs/?((?<job_index>\d+)/task/?)?} do
        @object = resolveObject()
        job = Occam::Job.new(:id      => params[:job_index],
                             :account => @object.account)

        info = job.taskInfo
        if not info
          status 404
          return
        end

        content_type "application/json"
        info.to_json
      end

      # Retrieve the job network instantiation
      get %r{#{OBJECT_ROUTE_REGEX}/jobs/?((?<job_index>\d+)/network/?)?} do
        @object = resolveObject()
        job = Occam::Job.new(:id      => params[:job_index],
                             :account => @object.account)

        info = job.networkInfo
        if not info
          status 404
          return
        end

        content_type "application/json"
        info.to_json
      end

      # Retrieve the job status
      get %r{#{OBJECT_ROUTE_REGEX}/jobs/?((?<job_index>\d+)/?)?} do
        @object = resolveObject()
        job = Occam::Job.new(:id      => params[:job_index],
                             :account => @object.account)

        content_type "application/json"
        {"job" => job.info}.to_json
      end

      # Retrieve the workflow run status
      get %r{#{OBJECT_ROUTE_REGEX}/runs/?((?<run_index>\d+)/?)?} do
        @object = resolveObject()
        # Pull out the object (as a workflow)
        workflow = resolveObject(Occam::Workflow)

        format = request.preferred_type(['application/json', 'text/html'])

        run = workflow.runInfo(:runID => params[:run_index])
        case format
        when 'text/html'
          jobs = run[:nodes].values.flatten.map{|jobMap| jobMap[:jobs].map{|job| Occam::Job.new(job)} }.flatten
          render :slim, :"objects/_job-list", :layout => !request.xhr?, :locals => {
            :jobs   => jobs,
            :object => @object,
          }
        else
          content_type "application/json"

          run.to_json
        end
      end

      # Retrieve the workflow run job list
      get %r{#{OBJECT_ROUTE_REGEX}/runs/(?<run_index>\d+)/((?<node_index>\d+)/?)} do
        @object = resolveObject()
        # Pull out the object (as a workflow)
        workflow = resolveObject(Occam::Workflow)

        format = request.preferred_type(['application/json', 'text/html'])

        run = workflow.runInfo(:runID => params[:run_index])

        # Determine if the node exists (and is known) to the run
        node_index = params["node_index"].to_s.intern

        # If not, return a 404
        if not run[:nodes].has_key?(node_index)
          status 404
          return
        end

        # Render the job listing
        case format
        when 'text/html'
          jobs = run[:nodes][node_index][:jobs].map{|job| Occam::Job.new(job) }
          render :slim, :"objects/_job-list", :layout => !request.xhr?, :locals => {
            :jobs   => jobs,
            :object => @object,
          }
        else
          content_type "application/json"

          run[:nodes][node_index].to_json
        end
      end

      # Gets the file content of the object
      get %r{#{OBJECT_ROUTE_REGEX}/file/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'text/html+configuration', 'application/json'])
        case format
        when "text/html+configuration"
          # Render the object data as a configuration form

          # First, retrieve the schema
          schema = @object.schema

          # Use this to render the form
          render :slim, :"objects/_configuration", :layout => !request.xhr?, :locals => {
            :reviewing           => @params[:params] && @params[:params].has_key?("review"),
            :object              => @object,
            :revision            => @object.revision,
            :schema              => schema,
            :configuration_index => 0,
            :index               => 0
          }
        else
          query = Rack::Utils.parse_query(request.query_string)
          if @object.info.has_key?(:file)
            redirect @object.url(:path => "/files/#{@object.info[:file]}", :query => query)
          else
            redirect @object.url(:path => "/files", :query => query)
          end
        end
      end

      post %r{#{OBJECT_ROUTE_REGEX}/publish/(?<backend>[^/]+)(/(?<id>[^/+]))?} do
        @object = resolveObject()
        if current_person.nil?
          status 404
          halt
        end

        globalStore = Occam::System::Storage.list[params[:backend].intern]

        if not globalStore
          status 404
          halt
        end

        if globalStore.accountInfo != {}
          store = Occam::System::Storage.view(params[:backend], current_account, params[:id]).first
          store.push(@object)
        else
          globalStore.push(@object)
        end
      end

      # Gets the contents of the object
      #
      # Returns a 404 if the object is not found.
      #
      # Will return an empty array if the object does not contain other objects.
      #
      # @!macro object_route
      # @param [String] extension The extension to force the content to render as.
      #                           This can also be set using the Accept HTTP header.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), application/json
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/contains
      #
      # Retrieves an HTML document that renders the list of contents.
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/contains.json
      #
      # Retrieves a JSON document that describes the contents of the object. Returns
      # an array of Object metadata. That metadata will have, at least, an 'id', 'uid', and
      # 'revision', and may include a 'name' and 'type'.
      get %r{#{OBJECT_ROUTE_REGEX}/contains(?<extension>\.json)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'application/json'])

        if params[:extension] == ".json"
          format = "application/json"
        end

        case format
        when 'application/json'
          content_type "application/json"

          @object.contents.map(&:info).to_json
        else
          render :slim, :"objects/_object_tree", :layout => !request.xhr?, :locals => {
            :root    => false,
            :type    => params["type"],
            :objects => @object.contents
          }
        end
      end

      # Retrieves a list of possible backends that can run this object natively.
      #
      # It will return an empty array if no backends are available.
      #
      # @!macro object_route
      #
      # === Accepted Content Types:
      #
      # application/json (_default_)
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/backends
      #
      # Returns a JSON document consisting of an array of dictionary blocks containing
      # information about each available backend.
      get %r{#{OBJECT_ROUTE_REGEX}/backends/?} do
        @object = resolveObject()
        content_type "application/json"

        @object.backends.to_json
      end

      get %r{/objects/select/?} do
        render :slim, :"objects/select", :layout => !request.xhr?, :locals => {
          :section => params[:section],
          :moduleName => params[:module],
          :subversion => params[:subversion],
          :type => params[:type],
          :errors => []
        }
      end
    end
  end

  use Controllers::ObjectController
end
# @!endgroup
