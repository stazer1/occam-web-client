# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group License API
class Occam
  module Controllers
    class LicenseController < Occam::Controller
      get "/licenses/?" do
        content_type "application/json"
        Occam::License.all.to_json
      end

      get "/licenses/:key/?" do
        format = request.preferred_type(['text/html', 'application/json', 'text/plain'])

        case format
        when 'application/json'
          ret = Occam::License.load(params[:key]).to_json
          content_type 'application/json'
          ret
        when 'text/plain'
          ret = Occam::License.text(params[:key])
          content_type "text/plain"
          ret
        else
          license = Occam::License.new(Occam::License.load(params[:key]))
          render :slim, :"objects/license", :layout => !request.xhr?, :locals => {
            :license => license,
            :object => nil
          }
        end
      rescue
        status 404
      end
    end
  end

  use Controllers::LicenseController
end
# @!endgroup
