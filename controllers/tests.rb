# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Test API
class Occam
  module Controllers
    # Handles routes based on viewing occam test information.
    class TestController < Occam::Controller
      # Get the test coverage for the tests, if it exists
      get '/tests/client/coverage-raw' do
        redirect '/tests/client/coverage-raw/'
      end

      get '/tests/client/coverage-raw/*' do
        begin
          subpath = params[:splat][0]
          basepath = File.join(File.dirname(__FILE__), "..", "spec", "coverage")
          basepath = File.realpath(basepath)

          if subpath == "/" or subpath == "" or subpath.nil?
            path = File.join(basepath, "index.html")
          else
            path = File.join(basepath, subpath)
          end

          path = File.realpath(path)

          if path.start_with?(basepath) and File.exist?(path)
            send_file path
          else
            status 404
          end
        rescue
          status 404
        end
      end

      get '/tests/javascript/coverage-raw' do
        redirect '/tests/javascript/coverage-raw/'
      end

      get '/tests/javascript/coverage-raw/*' do
        begin
          subpath = params[:splat][0]
          basepath = File.join(File.dirname(__FILE__), "..", "spec", "js", "coverage")
          basepath = File.realpath(basepath)

          if subpath == "/" or subpath == "" or subpath.nil?
            path = File.join(basepath, "index.html")
          else
            path = File.join(basepath, subpath)
          end

          path = File.realpath(path)

          if path.start_with?(basepath) and File.exist?(path)
            send_file path
          else
            status 404
          end
        rescue
          status 404
        end
      end

      get '/tests/daemon/coverage-raw' do
        redirect '/tests/daemon/coverage-raw/'
      end

      get '/tests/daemon/coverage-raw/*' do
        begin
          subpath = params[:splat][0]
          path = Occam::System.daemonPath("htmlcov")

          if subpath == "/" or subpath == "" or subpath.nil?
            path = File.join(path, "index.html")
          else
            path = File.join(path, subpath)
          end

          path = File.realpath(path)

          if path.start_with?(Occam::System.daemonPath()) and File.exist?(path)
            send_file path
          else
            status 404
          end
        rescue
          status 404
        end
      end

      # Get the test results for a particular project
      get '/tests/?:tab?/?' do
        testRun = Occam::TestRun.for(params[:tab])

        if request.xhr?
          if params[:tab].nil? || params[:tab] == "" || params[:tab] == "overall"
            render :slim, :"tests/_overview", :layout => !request.xhr?
          else
            render :slim, :"tests/_project", :layout => !request.xhr?, :locals => {
              :test_run => testRun
            }
          end
        else
          render :slim, :"tests/index", :layout => !request.xhr?, :locals => {
            :test_run => testRun
          }
        end
      end

      # Get the test results for a particular test group for a project
      get '/tests/:tab/:subtab*' do
        testRun = Occam::TestRun.for(params[:tab])

        if request.xhr?
          if params[:subtab].nil? || params[:subtab] == "" || params[:subtab] == "overall"
            render :slim, :"tests/_overall", :layout => !request.xhr?, :locals => {
              :test_run => testRun
            }
          elsif params[:subtab] == "coverage"
            render :slim, :"tests/_coverage", :layout => !request.xhr?, :locals => {
              :path => params[:splat][0]
            }
          else
            render :slim, :"tests/_unit-test", :layout => !request.xhr?, :locals => {
              :group => testRun && testRun.groups[params[:subtab].intern]
            }
          end
        else
          render :slim, :"tests/index", :layout => !request.xhr?, :locals => {
            :test_run => testRun,
            :path => params[:splat][0]
          }
        end
      end
    end
  end

  use Controllers::TestController
end
# @!endgroup
