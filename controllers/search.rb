# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Search API
class Occam
  module Controllers
    # Performs a search.
    class SearchController < Occam::Controller
      options '/search' do
        handleCORS('*')
        status 200
      end

      # Performs a search with the given query and filters.
      get '/search' do
        handleCORS('*')

        searchResult = performSearch(:excludeTypes => ["task"])

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        case format
        when 'application/json'
          {
            "types" => searchResult[:types].map do |type|
              type.update(:icon => Occam::Object.iconURLFor(type[:type]),
                          :smallIcon => Occam::Object.iconURLFor(type[:type], :small => true))
            end,
            "objects" => searchResult[:objects].map do |object|
              object.info.update(:revision    => object.revision,
                                 :icon        => object.iconURL,
                                 :iconID      => object.iconID,
                                 :smallIcon   => object.iconURL(:small => true),
                                 :smallIconID => object.iconID(:small => true))
            end
          }.to_json
        when 'text/html'
          render :slim, :"search/results", :layout => !request.xhr?, :locals => searchResult
        end
      end
    end
  end

  use Controllers::SearchController
end
# @!endgroup
