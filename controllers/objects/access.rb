# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all permission requests for an object.
    class ObjectAccessController < Occam::Controllers::ObjectController
      # Displays the access tab.
      get %r{#{OBJECT_ROUTE_REGEX}/access/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html'])

        case format
        when 'text/html'
          renderObjectView("access")
        end
      end

      # Updates object permissions
      post %r{#{OBJECT_ROUTE_REGEX}/permissions/?} do
        @object = resolveObject()
        key     = params["update"] || "read"
        value   = params["value"] || "on"
        identity = params["identity"] || params["object-identity"]
        section = params["section"]

        if value == ""
          value = nil
        else
          value = value == "on"
        end

        # Update permission
        row = @object.setPermission(key, value, section == "children", identity)

        if request.xhr?
          render :slim, :"objects/permissions/_row",
                        :layout => false,
                        :locals => {
            :access  => row,
            :section => section,
            :object  => @object,
          }
        else
          if request.referrer
            redirect request.referrer
          else
            redirect @object.url(:path => "access")
          end
        end
      end

      # Deletes the permissions for a particular person.
      delete %r{#{OBJECT_ROUTE_REGEX}/permissions/?} do
        @object = resolveObject()
        identity = params[:identity]
        section = params[:section]

        # Delete permission row
        @object.resetPermission(section == "children", identity)

        if request.xhr?
        else
          if request.referrer
            redirect request.referrer
          else
            redirect @object.url(:path => "access")
          end
        end
      end
    end
  end

  use Controllers::ObjectAccessController
end
