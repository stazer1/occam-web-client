# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all configuration saving for a specific object.
    class ObjectConfigurationsController < Occam::Controllers::ObjectController
      # Retrieves the saved configuration for the current account.
      #
      # Returns a 404 if the object is not found.
      #
      # @!macro object_route
      # @param [String] extension The extension to force the content to render as.
      #                           This can also be set using the Accept HTTP header.
      #
      # === Accepted Content Types:
      #
      # text/html (_default_), application/json
      #
      # === Examples:
      #
      #   GET /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/configurations/0
      #
      get %r{#{OBJECT_ROUTE_REGEX}/configurations/(?<wire>\d+)(?<extension>\.json)?/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html', 'application/json'])

        # Determine the input index to render
        index = params[:wire].to_i

        inputs = @object.info[:inputs] || []

        if 0 > index || inputs.length <= index
          status 404
          return
        end

        input = inputs[index]

        if params[:extension] == ".json"
          format = "application/json"
        end

        case format
        when 'application/json'
          content_type 'application/json'
          @object.defaultConfiguration(params[:wire]).to_json
        else
          # First, retrieve the schema (if it is a file)
          schema = input[:schema]
          if !schema.is_a?(Hash)
            schema = @object.retrieveJSON(input[:schema])
          end

          data = @object.defaultConfiguration(params[:wire])
          puts "getting data", data

          # Use this to render the form
          return render :slim, :"objects/_configuration", :layout => !request.xhr?, :locals => {
            :reviewing           => @params[:params] && @params[:params].has_key?("review"),
            :object              => @object,
            :revision            => @object.revision,
            :schema              => schema,
            :configuration_index => index,
            :index               => nil,
            :data                => data
          }
        end
      end

      # Writes any new configuration data.
      #
      # @!macro object_route
      #
      # === Accepted Content Types:
      #
      # text/html (_default_)
      #
      # === Examples:
      #
      #   POST /QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/configurations/0
      post %r{#{OBJECT_ROUTE_REGEX}/configurations/(?<wire>\d+)/?} do
        @object = resolveObject()

        # Write the data out to the object
        puts params[:nullify]
        puts params[:data]

        puts "blah"
        Occam::ObjectInfo.removeNullify(params[:data], params[:nullify]);
        data = Occam::Workflow.decode_base64(params[:data])
        puts "new data", data
        @object.setDefaultConfiguration(params[:wire], data)

        # Redirect back to the configuration
        redirectURL = @object.url(:path => "configurations/#{params[:wire]}")

        # Gather format requested
        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        case format
        when 'application/json'
          # Ajax call, generally
          content_type "application/json"
          {
            :url => redirectURL
          }.to_json
        else
          redirect redirectURL
        end
      end
    end
  end

  use Controllers::ObjectConfigurationsController
end
