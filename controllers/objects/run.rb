# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../objects"

# @api api
# @!group Object API
class Occam
  module Controllers
    # This controller handles all run requests for an object.
    class ObjectRunController < Occam::Controllers::ObjectController
      # Displays an interface for running the object.
      get %r{#{OBJECT_ROUTE_REGEX}/run/?} do
        @object = resolveObject()
        format = request.preferred_type(['text/html'])

        case format
        when 'text/html'
          renderObjectView("run")
        end
      end

      # Retrieves the run queue form for setting up options for running the object.
      get %r{#{OBJECT_ROUTE_REGEX}/run/queue(?<path>.+)?} do
        # Resolve to parse path parameter
        @object = resolveObject()

        # Render a task generation form for this object for running
        render :slim, :"objects/_run-form", :layout => false, :locals => {
          :object => @object,
          :phase => :run,
          :running => true
        }
      end

      # Displays the run tasks known for this object.
      get %r{#{OBJECT_ROUTE_REGEX}/run/tasks/?} do
        @object = resolveObject()

        content_type 'application/json'
        @object.tasks.to_json
      end

      # Queue a run
      post %r{#{OBJECT_ROUTE_REGEX}/runs/?} do
        @object = resolveObject()

        if params.has_key?(:published)
          @object = @object.as(Occam::Object, :link => nil)
        end

        input = nil
        if params[:inputs]
          input = Occam::Object.new(:id       => params[:inputs],
                                    :account  => @object.account)
        end

        if params[:commit]
          @object = @object.commit
          @object = @object.as(Occam::Object, :link => nil)
        end

        # Pull out the object (as a workflow, if needed)
        if @object.info[:type] == "workflow" || @object.info[:type] == "experiment"
          workflow = @object.as(Occam::Workflow)
          runInfo = workflow.queue(:target => params[:target])
          run = Occam::Run.new(:id => runInfo[:id], :info => runInfo)
        else
          interactive = params[:interactive] == true || params[:interactive] == "true"
          run = @object.run(:input => input,
                            :interactive => interactive,
                            :published => params.has_key?(:published),
                            :target => params[:target])
        end

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        case format
        when 'application/json'
          content_type "application/json"
          run.to_json
        else
          render :slim, :"objects/_run-list-item", :layout => false, :locals => {
            :run => run,
            :staged => (@object.link ? "run" : nil),
            :object => @object
          }
        end
      end

      # Cancels the run
      post %r{#{OBJECT_ROUTE_REGEX}/runs/(?<run_index>\d+)/(?<action>\w+)/?} do
        @object = resolveObject()
        if (params[:action] == "cancel")
          # Pull out the object (as a workflow)
          workflow = resolveObject(Occam::Workflow)

          runInfo = workflow.runInfo(:runID => params[:run_index])
          run = Occam::Run.new(:id => runInfo[:run][:id],
                               :info => runInfo,
                               :account => @object.account)


          run.cancel
        else
          status 400
          return
        end
        if request.xhr?
          content_type "application/json"
          run.info.to_json
        elsif request.referrer
          redirect request.referrer
        end
      end
    end
  end

  use Controllers::ObjectRunController
end
