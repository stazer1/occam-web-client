# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group People API
class Occam
  module Controllers
    class PersonController < Occam::Controller
      def renderPersonView(tab, options={})
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        params[:tab] = tab

        if !person || !person.exists?
          status 404
        else
          if request.xhr? && params[:tab]
            render :slim, :"people/_#{params[:tab]}", :layout => false, :locals => {
              :errors => nil,
              :tab    => params[:tab],
              :help   => params['help'],
              :person => person
            }.update(options)
          else
            render :slim, :"people/show", :layout => !request.xhr?, :locals => {
              :errors => nil,
              :tab    => params[:tab],
              :help   => params['help'],
              :person => person
            }.update(options)
          end
        end
      end

      # Searches for people known to this system or federation.
      get '/people' do
        redirect '/search?type=person'
      end

      # Form to create a new person and account on the local system.
      get '/people/new' do
        # Do not allow account creation if signup is disallowed
        if !Occam::Config.configuration['allow-signup']
          status 404
          return
        end

        pwPolicyMarkdown = Occam::System.local.passwordPolicyMarkdown()

        render :slim, :"people/new", :layout => !request.xhr?, :locals => {
          :errors => nil, :policies => Occam::System.policies,
          :pwPolicyMarkdown => pwPolicyMarkdown}
      end

      # Creates an account with the given username and password.
      post '/people' do
        # Do not allow account creation if signup is disallowed
        if !Occam::Config.configuration['allow-signup']
          status 404
          return
        end

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        # TODO: Put this into the system info as well.
        policies = Occam::System.policies

        systemInfo = Occam::System.local.info()
        pwPolicyMarkdown = Occam::System.local.passwordPolicyMarkdown()

        if systemInfo['passwordPolicy'.to_sym].nil? || systemInfo['passwordPolicy'.to_sym] == {}
          passwordPolicy = nil
        else
          passwordPolicy = systemInfo['passwordPolicy'.to_sym].map{ |k, v| [k, v].join(': ')}
        end

        if params["newGroup"]
          if !logged_in?
            status 406
            return
          end

          person = current_person.newPerson(params["username"], [params["subtype"]])
          redirect person.url
        else

          # Make sure the user agrees to follow the policies.
          policiesAgreedTo = []
          params.each do |k, v|
            if k =~ /^policy_/
              policiesAgreedTo.push(k[7..])
            end
          end
          policies.each do |k, policyDetails|
            if !policiesAgreedTo.include?(k)
              status 422
              case format
              when 'application/json'
                return {
                  'errors': [I18n.t("policies.mustAgreeToPolicies")]
                }.to_json
              else
                return render :slim, :"people/new", :layout => !request.xhr?, :locals => {
                  :errors => [I18n.t("policies.mustAgreeToPolicies")],
                  :policies => policies, :pwPolicyMarkdown => pwPolicyMarkdown
                }
              end
            end
          end

          username = params["username"]
          password = params["password"]
          # If the field is left blank, the email param will be "". If so we want nil.
          # Email validity is checked on the backend, but we'll strip whitespace here.
          email    = params["email"].strip == "" ? nil : params["email"].strip
          ipAddress = request.ip

          account = Occam::Account.create(username, password, ipAddress, email)
          login(username, password, ipAddress)

          case format
          when 'application/json'
            content_type "application/json"
            {
              :url => account.person.url
            }.to_json
          else
            redirect account.person.url
          end
        end
      rescue Occam::Daemon::Error => e
        status 422

        case format
        when 'application/json'
          {
            'errors': [e.to_s]
          }.to_json
        else
          render :slim, :"people/new", :layout => !request.xhr?, :locals => {
            :errors => [e], :policies => policies
          }
        end
      end

      # Adds an object to the list of recently used objects
      post '/people/:identity/recentlyUsed' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if !person || person.id != current_person.id
          # TODO: confirm this is not a 406
          status 404
          return
        end

        # Get the object
        object_id = params["object_id"]
        object = Occam::Object.new(:id => object_id, :account => current_account)

        if !object.exists?
          # TODO: this is likely an argument error
          status 404
          return
        end

        # Create recently used link
        current_person.createRecentlyUsed(object)

        if request.xhr?
        else
          if request.referrer
            redirect request.referrer
          else
            redirect object.url
          end
        end
      end

      get '/people/:identity/runs/running' do
        "{}"
      end

      # Form to update a person's password
      get '/people/:identity/password' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if not person.exists?
          status 404
        else
          # TODO: allow password editing for administrators
          if current_person.id != person.id
            status 406
          else
            pwPolicyMarkdown = Occam::System.local.passwordPolicyMarkdown()
            render :slim, :"people/update-password", :layout => !request.xhr?, :locals => {
              :errors  => nil,
              :person => current_person,
              :pwPolicyMarkdown => pwPolicyMarkdown
            }
          end
        end
      end

      # Update the person's current password
      post '/people/:identity/password' do
        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)
        ipAddress = request.ip

        if not person.exists?
          status 404
        else
          # TODO: allow password editing for administrators
          if current_person.id != person.id
            status 406
          else
            begin
              current_account.updatePassword(params[:old_password], params[:password], ipAddress)

              # If the user opened a new tab to reset their password, send them
              # to their personal page. Otherwise just redirect to the
              # referrer, this will keep them on their personal profile in the
              # keys tab.
              referrer = request.referrer
              if referrer
                begin
                  referrer = URI::parse(referrer)
                rescue
                  referrer = nil
                end
              end
              if referrer && !referrer.path.end_with?("password")
                redirectURL = referrer
              else
                redirectURL = current_account.person.url
              end

              # If we got sent an async request expecting JSON, provide it.
              # Otherwise we'll want to just redirect directly.
              case format
              when 'application/json'
                content_type "application/json"
                {
                  :url => redirectURL
                }.to_json
              else
                redirect redirectURL
              end
            rescue Occam::Daemon::Error => e
              status 422

              # Make sure modals add errors directly instead of redirecting to
              # the non-modal page.
              case format
              when 'application/json'
                return {
                  'errors': [e.to_s]
                }.to_json
              else
                pwPolicyMarkdown = Occam::System.local.passwordPolicyMarkdown()
                render :slim, :"people/update-password", :layout => !request.xhr?, :locals => {
                  :errors  => [e],
                  :person => current_person,
                  :pwPolicyMarkdown => pwPolicyMarkdown
                }
              end
            end
          end
        end
      end

      # Form to reset an account password
      get '/people/:identity/reset-password/:token' do
        pwPolicyMarkdown = Occam::System.local.passwordPolicyMarkdown()
        render :slim, :"people/reset-password", :layout => !request.xhr?, :locals => {
          :errors => nil,
          :reset_token => params[:token],
          :identity => params[:identity],
          :username => params[:username],
          :pwPolicyMarkdown => pwPolicyMarkdown
        }
      end

      # Commit an account password reset
      post '/people/:identity/reset-password' do
        reset_token = params[:token]
        identity = params[:identity]
        new_password = params[:new_password]
        ipAddress = request.ip
        pwPolicyMarkdown = Occam::System.local.passwordPolicyMarkdown()

        if new_password != params[:confirm_new_password]
          # Re-render the form and error when the password does not match the confirm field
          render :slim, :"people/reset-password", :layout => !request.xhr?, :locals => {
            :errors => [I18n.t("people.resetPassword.errors.passwordMismatch")],
            :reset_token => params[:token],
            :identity => params[:identity],
            :username => params[:username],
            :pwPolicyMarkdown => pwPolicyMarkdown
          }
        else
          # Reset the password
          Occam::Account.resetPassword(reset_token, identity, new_password, ipAddress)

          # Redirect to the login screen
          redirect "/login?username=#{params[:username]}&flash=people.resetPassword.success"
        end
      rescue Occam::Daemon::Error => e
        status 422

        # An error meant there was a failure to either get the password policy
        # or reset the password. Attempt to supply the policy, but don't let
        # failure to do so prevent rendering the form.
        pwPolicyMarkdown = nil
        begin
          pwPolicyMarkdown = Occam::System.local.passwordPolicyMarkdown()
        rescue Occam::Daemon::Error => e
        end

        render :slim, :"people/reset-password", :layout => !request.xhr?, :locals => {
          :errors => [e],
          :reset_token => params[:token],
          :identity => params[:identity],
          :username => params[:username],
          :pwPolicyMarkdown => pwPolicyMarkdown
        }
      end

      # Validate email
      get '/people/:identity/validate-email' do
        email_token = params[:token]
        identity = params[:identity]
        person = Occam::Person.fromIdentity(params[:identity])

        if Occam::Account.validateEmail(email_token, identity)
          if not current_account.nil? and identity == current_account.identity
            redirect "/people/#{identity}?flash=people.validateEmail.header"
          else
            redirect "/login?flash=people.validateEmail.header"
          end
        else
          status 406
        end
      end
    end
  end

  use Controllers::PersonController
end
# @!endgroup
