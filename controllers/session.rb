# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
# @!group Session API
class Occam < Sinatra::Base
  module Controllers
    class SessionController < Occam::Controller
      # Retrieve login form.
      get '/login' do
        render :slim, :"sessions/login", :layout => !request.xhr?, :locals => {
          :errors => nil,
          :username_or_email => params[:username_or_email]
        }
      end

      # Sign on
      post '/login' do
        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])
        ipAddress = request.ip

        begin
          account = login(params["username_or_email"], params["password"], ipAddress)
        rescue Exception => e
          status 422

          e = e.message

          case format
          when 'application/json'
            return {
              'errors': [e.to_s]
            }.to_json
          else
            return render :slim, :"sessions/login", :layout => !request.xhr?, :locals => {
              :errors => [e]
            }
          end
        end

        referrer = request.referrer
        if referrer
          begin
            referrer = URI::parse(referrer)
          rescue
            referrer = nil
          end
        end

        redirectURL = nil

        if referrer && !referrer.path.end_with?("login") && (referrer.path != "/" && referrer.path != "/about")
          redirectURL = request.referrer
        else
          # Redirect to their personal page
          redirectURL = account.person.url
        end

        case format
        when 'application/json'
          content_type "application/json"
          {
            :url => redirectURL
          }.to_json
        else
          redirect redirectURL
        end
      rescue Occam::Daemon::Error => e
        status 422

        case format
        when 'application/json'
          {
            'errors': [e.to_s]
          }.to_json
        else
          render :slim, :"sessions/login", :layout => !request.xhr?, :locals => {
            :errors => [e]
          }
        end
      end

      # Retrieve forgot password form
      get '/forgot-password' do
        render :slim, :"sessions/forgot-password", :layout => !request.xhr?, :locals => {:errors => nil}
      end

      # Forgot password
      post '/forgot-password' do
        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        Occam::Account.forgotPassword(params["username_or_email"])

        case format
        when 'application/json'
          content_type "application/json"
          {
            :url => '/email-sent'
          }.to_json
        else
          redirect '/email-sent'
        end
      rescue Occam::Daemon::Error => e
        status 422

        case format
        when 'application/json'
          {
            'errors': [e.to_s]
          }.to_json
        else
          render :slim, :"sessions/forgot-password", :layout => !request.xhr?, :locals => {
            :errors => [e]
          }
        end
      end

      # Forgot password confirmation
      get '/email-sent' do
        render :slim, :"sessions/email-sent", :layout => !request.xhr?, :locals => {:errors => nil}
        # This should be a modal just like the login and forgot pass forms
      end

      # Sign out
      get '/logout' do
        logout

        redirect '/'
      end
    end
  end

  use Controllers::SessionController
end
# @!endgroup
