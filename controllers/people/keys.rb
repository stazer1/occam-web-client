# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle the managing of bookmarks for the logged in Person.
    class PersonKeysController < Occam::Controllers::PersonController
      # Renders the bookmark listing
      get '/people/:identity/keys/?' do
        renderPersonView("keys")
      end
    end
  end

  use Controllers::PersonKeysController
end
