# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle the maintenance of associations for a Person.
    class PersonAssociationsController < Occam::Controllers::PersonController
      # Renders the bookmark listing
      get '/people/:identity/associations/?' do
        renderPersonView("associations")
      end

      # Add association
      post '/people/:identity/associations/:kind' do
        # Do not allow a non-logged in account to mess with this
        if current_person.nil? || params[:identity] != current_person.identity.uri
          status 404
        end

        obj = Occam::Object.new(:id => params[:object_id], :revision => params[:revision], :info => {})

        current_person.addAssociation(params[:kind], params[:major], obj, :minor => params[:minor],
                                                                          :systemWide => params[:system_wide])

        redirect request.referrer
      end

      # Remove associations
      delete '/people/:identity/associations/:kind' do
        # Do not allow a non-logged in account to mess with this
        if current_person.nil? || params[:identity] != current_person.identity.uri
          status 404
        end

        obj = Occam::Object.new(:id => params[:object_id], :revision => params[:revision], :info => {})

        current_person.removeAssociation(params[:kind], params[:major], obj, :minor => params[:minor])

        redirect request.referrer
      end
    end
  end

  use Controllers::PersonAssociationsController
end
