# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle the managing of highlighted objects for the logged in Person.
    class PersonHighlightsController < Occam::Controllers::PersonController
      # Deletes a highlighted item from the highlights list
      delete '/people/:identity/highlights/:target' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if !person || person.identity.uri != current_person.identity.uri
          # TODO: confirm this is not a 406
          status 404
          return
        end

        # Craft the link
        link = Occam::Link.new(:target       => Occam::Object.new(:id => params[:target]),
                               :relationship => "highlight",
                               :account      => current_account,
                               :source       => current_person)

        # Destroy the link
        link.destroy!

        if request.xhr?
        else
          if request.referrer
            redirect request.referrer
          else
            redirect object.url
          end
        end
      end

      # Highlights objects
      post '/people/:identity/highlights' do
        person = Occam::Person.fromIdentity(params[:identity],
                                            :account  => current_account)

        if !person || person.identity.uri != current_person.identity.uri
          # TODO: confirm this is not a 406
          status 404
          return
        end

        # Get the object
        object_id = params["object_id"]
        object = Occam::Object.new(:id => object_id,
                                   :account => current_account)

        if !object.exists?
          # TODO: this is likely an argument error
          status 404
          return
        end

        # Create highlight link
        current_person.highlight(object)

        if request.xhr?
        else
          if request.referrer
            redirect request.referrer
          else
            redirect object.url
          end
        end
      end
    end
  end

  use Controllers::PersonHighlightsController
end
