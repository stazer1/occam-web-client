# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative "../people"

# @api api
# @!group People API
class Occam
  module Controllers
    # These routes handle the managing of the active objects for the logged in Person.
    class PersonUploadsController < Occam::Controllers::PersonController
      # Opens the listing of current uploads
      get '/people/:identity/uploads/?' do
        renderPersonView("uploads")
      end

      # Opens the form to create an object from a file or external resource
      get '/people/:identity/uploads/new' do
        render :slim, :"objects/upload", :layout => !request.xhr?, :locals => {
          :errors => []
        }
      end

      # Creates a new object and adds it to the uploaded list.
      post '/people/:identity/uploads' do
        if current_person.nil? || params[:identity] != current_person.identity.uri
          status 404
          return
        end

        format = request.preferred_type(['text/html', 'application/json', 'application/atom+xml', 'application/xml'])

        # The base object info for the wrapped object
        type = params[:type]
        name = params[:name]
        filename = nil
        object = nil

        # Gather the file data to pass it through to create the resource
        if params[:file]
          # Parse JSON for file upload
          filename = params[:file][:filename]

          if name.nil? || name == ""
            name = filename
          end

          # TODO: allow streaming files to stdin
          data = params[:file][:tempfile].read
          resourceInfo = Occam::Resource.create(:type    => "file",
                                                :name    => filename,
                                                :data    => data,
                                                :account => current_account)
          resourceInfo[:to] = filename
          resource = Occam::Resource.new(:id       => resourceInfo[:id],
                                         :uid      => resourceInfo[:uid],
                                         :revision => resourceInfo[:revision],
                                         :info     => resourceInfo)
        end

        if filename
          # Get the file type
          report = resource.analysis(:filename => filename)

          if type.nil? || type == ""
            type = report[:report][:type]
          end

          subtype = report[:report][:subtype]

          info = {
            :subtype => subtype,
            :install => [
              resourceInfo
            ],
            :file => filename
          }

          # Attempt to find any existing object that also wraps that upload
          object = current_person.uploadFor(resourceInfo, type, name)

          # Create a new object with the resource
          if object.nil?
            object = Occam::Object.create(:name    => name,
                                          :type    => type,
                                          :account => current_account,
                                          :info    => info)
          end
        end

        if object.nil?
          status 422
          return
        end

        # Link to the object via uploads
        current_person.createLink(:relationship => "uploaded",
                                  :object       => object,
                                  :account      => current_account)
        current_person.uploaded(resourceInfo, object)

        case format
        when 'application/json'
          content_type "application/json"
          {
            :url => object.url,
            :icon => {
              :url => object.iconURL,
              :id => object.iconID
            },
            :object => object.info(true).update({:id => object.id,
                                                 :uid => object.uid,
                                                 :revision => object.revision})
          }.to_json
        else
          redirect object.url
        end
      rescue Occam::Daemon::Error => e
        status 422

        case format
        when 'application/json'
          {
            'errors': [e.to_s]
          }.to_json
        else
          render :slim, :"objects/upload", :layout => !request.xhr?, :locals => {:errors => [e]}
        end
      end
    end
  end

  use Controllers::PersonUploadsController
end
