# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# @api api
class Occam
  # This will redirect the ACM demo object from v0 IDs to the correct ID
  get '/objects/c25f1daa-2cc0-11e6-ac2e-000af7451cc2' do
    redirect "/QmZkPDC6QgaeR6DK7CiYQ4CNU2ZvPXHUdH5hcGsGEUzLSi/5dstZQTtYkHVawf1N87aNkJYF3DpJB?" + request.query_string
  end
end
