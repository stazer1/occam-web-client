const fs = require('fs');
const path = require('path');

const SVGO = require('svgo');

const imagemin = require('imagemin');
const imageminJpegtran = require('imagemin-jpegtran');
const imageminPngquant = require('imagemin-pngquant');
const imageminWebp = require('imagemin-webp');

// Minimize the remaining static images

console.log();
console.log("Minimizing static images...");

// Initialize SVGO

svgo = new SVGO({
    plugins: [{
        cleanupAttrs: true,
    }, {
        removeDoctype: true,
    },{
        removeXMLProcInst: true,
    },{
        removeComments: true,
    },{
        removeMetadata: true,
    },{
        removeTitle: true,
    },{
        removeDesc: true,
    },{
        removeUselessDefs: true,
    },{
        removeEditorsNSData: true,
    },{
        removeEmptyAttrs: true,
    },{
        removeHiddenElems: true,
    },{
        removeEmptyText: true,
    },{
        removeEmptyContainers: true,
    },{
        removeViewBox: false,
    },{
        cleanupEnableBackground: true,
    },{
        convertStyleToAttrs: true,
    },{
        convertColors: true,
    },{
        convertPathData: true,
    },{
        convertTransform: true,
    },{
        removeUnknownsAndDefaults: true,
    },{
        removeNonInheritableGroupAttrs: true,
    },{
        removeUselessStrokeAndFill: true,
    },{
        removeUnusedNS: true,
    },{
        cleanupIDs: true,
    },{
        cleanupNumericValues: true,
    },{
        moveElemsAttrsToGroup: true,
    },{
        moveGroupAttrsToElems: true,
    },{
        collapseGroups: true,
    },{
        removeRasterImages: false,
    },{
        mergePaths: true,
    },{
        convertShapeToPath: true,
    },{
        sortAttrs: true,
    },{
        removeDimensions: true,
    }]
});

// Add SVG source files
let basepath = 'images';

async function minimize(filename) {
    console.log("Minimizing", filename);

    await new Promise( async (resolve, reject) => {
        if (filename.endsWith('.svg')) {
            fs.readFile(filename, 'utf8', (err, data) => {
                svgo.optimize(data, { path: filename }).then( (result) => {
                    fs.mkdirSync("public/" + path.dirname(filename), { recursive: true });
                    fs.writeFileSync("public/" + filename,  result.data);
                    resolve();
                });
            });
        }
        else {
            let destination = "public/" + path.dirname(filename);
            fs.mkdirSync(destination, { recursive: true });
            await imagemin([filename], {
                destination: destination,
                plugins: [
                    imageminJpegtran(),
                    imageminPngquant(),
                ]
            });
            await imagemin([filename], {
                destination: destination,
                plugins: [
                    imageminWebp({quality: 50}),
                ]
            });

            resolve();
        }
    });
}

async function walk(subpath) {
    console.log("Crawling", subpath);

    await new Promise( (resolve, reject) => {
        fs.readdir(subpath, async (err, items) => {
            await new Promise( (resolve, reject) => {
                let count = items.length;
                items.forEach( async (item) => {
                    let filename = subpath + '/' + item;
                    await new Promise( (resolve, reject) => {
                        fs.stat(filename, async (err, stat) => {
                            if (stat && stat.isDirectory()) {
                                await walk(filename);
                            }
                            else {
                                await minimize(filename);
                            }
                            resolve();
                        });
                    });

                    count--;
                    if (count == 0) {
                        resolve();
                    }
                });
            });

            resolve();
        });
    });
}

new Promise( async (resolve, reject) => {
    await walk(basepath);

    resolve();
}).then( () => {
    // Some images need to not be optimized
    console.log();
    console.log("Copying not-to-be-optimized assets...");
    console.log("Copying images/indicators/object.svg");
    fs.copyFileSync("images/indicators/object.svg", "public/images/indicators/object.svg");

    console.log();
    console.log("Done.");
});
