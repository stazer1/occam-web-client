const webpack = require('webpack');
const path = require('path');
const { ESBuildMinifyPlugin } = require('esbuild-loader');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const ImageminWebpWebpackPlugin= require("imagemin-webp-webpack-plugin");
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
const fs = require('fs');

/* This describes the behavior of webpack.
 */
module.exports = {
  /* The main javascript file for the application
   */
  entry: {
    occam: ["./public/js/occam.js", "./stylesheets/app.scss"],
    vendor: ["./public/js/vendor.js", "./public/js/vendor.scss"],
    xterm: ["./public/js/vendor-xterm.js", "./public/js/vendor-xterm.scss"],
    easepick: ["./public/js/vendor-easepick.js", "./public/js/vendor-easepick.scss"],
    "polyfill-ie": ["./public/js/polyfill-ie.js", "./stylesheets/ie.scss"],
  },

  /* Just... ignore this for now.
   */
  performance: {
    hints: false
  },

  /* The default mode.
   */
  mode: "production",

  /* The eventual transpiled output file.
   */
  output: {
    path: __dirname + "/public/js",
    filename: "compiled-[name].js",
    sourceMapFilename: "[file].map"
  },

  /* We want source maps!
   */
  devtool: "source-map",

  /* What file types to filter.
   */
  resolve: {
    extensions: ['.js', '.css', '.scss', '.jpeg', '.jpg', '.png'],
  },

  /* How to load/import modules (for each file).
   */
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [/public\/js/, /node_modules\/@easepick/],
        loader: "esbuild-loader",
        options: {
          target: 'es2015'
        }
      },
      {
        test: /\.s?css$/,
        use: [
          {
            loader: 'style-loader'
          },
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '/public/css',
              esModule: false
            }
          },
          {
            loader: 'css-loader'
          },
          {
            loader: "sass-loader",
            options: {
              implementation: require("node-sass"),
              sassOptions: (loaderContext) => {
                // More information about available properties https://webpack.js.org/api/loaders/
                const { resourcePath, rootContext } = loaderContext;
                const relativePath = path.relative(rootContext, resourcePath);

                if (relativePath == "stylesheets/app.scss") {
                  return {
                    includePaths: [path.resolve(__dirname, 'stylesheets')],
                  };
                }
                else {
                  return {
                    includePaths: [path.resolve(__dirname, 'node_modules')],
                  };
                }
              }
            }
          }
        ]
      },
      {
        test: /\.(jpe?g|png|gif)/,
        loader: 'file-loader',

        options: {
          name: '[name].[ext]?[hash]',
          outputPath: 'public/'
        }
      }
    ]
  },

  /* Minimize all vendored css */
  optimization: {
    minimizer: [
      new ESBuildMinifyPlugin({
        target: 'es2015',
        css: true
      })
    ]
  },

  /* What plugins to use.
   */
  plugins: [
    new MiniCssExtractPlugin({
      // Options similar to the same options in webpackOptions.output
      // both options are optional
      filename: "../css/[name].css",
      chunkFilename: "[id].css"
    }),
    new ImageminWebpWebpackPlugin({
      config: [{
        test: /\.(jpe?g|png)/,
        options: {
          quality:  75
        }
      }],
      overrideExtension: true,
      detailedLogs: true,
      silent: false,
      strict: true
    }),
    new MomentLocalesPlugin(),
  ]
}
