#!/bin/bash
set -eu

mkdir -p ./vendor

pkgdir="$PWD/vendor"
source=https://github.com/npm/cli

export LD_LIBRARY_PATH=$PWD/vendor/lib:${LD_LIBRARY_PATH-}
export PATH=$PWD/vendor/bin:${PATH-}

rootdir=$PWD

cd "$pkgdir"

if [ ! -d npm ]; then
  git clone $source npm
fi

cd npm
./configure \
    --prefix="${pkgdir}" || exit -1
make -j4 || exit -1
make install || exit -1

cd $rootdir
