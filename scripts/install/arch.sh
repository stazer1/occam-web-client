#!/bin/bash

# Install required packages

# Package Dependencies
BINARY_PACKAGES=(ruby ruby-irb make autoconf automake gettext pkg-config gcc nodejs npm)
BINARY_CHECK=(ruby irb make autoconf automake gettext pkg-config gcc node npm)
LIBRARY_PACKAGES=(libffi libpng libjpeg giflib)
LIBRARY_CHECK=(libffi libpng libjpeg libgif)
INCLUDE_PACKAGES=()
INCLUDE_CHECK=()

# Stores the packages we need to install
PACKAGES=()

for i in "${!BINARY_CHECK[@]}"; do
  binary=${BINARY_CHECK[$i]}
  package=${BINARY_PACKAGES[$i]}
  if hash ${binary} 2>/dev/null; then
    echo "${package} already installed"
  else
    PACKAGES+=(${package})
  fi
done

for i in "${!LIBRARY_CHECK[@]}"; do
  library=${LIBRARY_CHECK[$i]}
  package=${LIBRARY_PACKAGES[$i]}
  if pkg-config ${library} --libs 2>/dev/null >/dev/null; then
    echo "${package} already installed"
  else
    if ls /usr/lib/${library}.so 2>/dev/null >/dev/null; then
      echo "${package} already installed"
    else
      PACKAGES+=(${package})
    fi
  fi
done

for i in "${!INCLUDE_CHECK[@]}"; do
  include=${INCLUDE_CHECK[$i]}
  package=${INCLUDE_PACKAGES[$i]}
  if find ${include} 2>/dev/null >/dev/null; then
    echo "${package} already installed"
  else
    PACKAGES+=(${package})
  fi
done

if [ ${#PACKAGES[@]} -ne 0 ]; then
  echo
  echo "Please run the following command as root to install the required packages and re-run the install script."
  echo

  echo pacman -Sy ${PACKAGES[@]} --noconfirm -q

  exit 1
fi

# Setup from here
./scripts/install/common.sh
