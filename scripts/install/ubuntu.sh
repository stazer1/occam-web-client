#!/bin/bash

# Install required packages
sudo apt install zlib1g-dev ruby-dev build-essential libffi-dev libssl-dev nodejs
sudo apt install npm

# Setup from here
./scripts/install/common.sh
