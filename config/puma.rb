require 'puma'

require_relative "../lib/config"
require_relative "../lib/https"

pwd = Dir.pwd

pidfile "#{pwd}/puma.pid"

puts "Running server..."

config = Occam::Config.configuration

host = config["host"] || "0.0.0.0"
port = ENV['OCCAM_WEB_PORT'] || config["port"] || 9292

workers config["workers"]
threads config["min-threads"], config["max-threads"]

Occam::HTTPS.ensureKey

bind "tcp://#{host}:#{port}?key=#{pwd}/key/server.key&cert=#{pwd}/key/server.crt"
