# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This module implements the creation of a self-signed certificate for
  # providing nodes with HTTPS access for the pulling of resources.
  module HTTPS
    require 'openssl'

    CRT_FILENAME = "key/server.crt"
    KEY_FILENAME = "key/server.key"

    # Creates an RSA SSL key for the server.
    def self.createKey
      # TODO: configurations
      # TODO: pull out organization info from the System tag

      name = "/C=US/ST=Pennsylvania/L=Pittsburgh/O=University of Pittsburgh/OU=Unit/CN=10.0.0.1"

      ca  = OpenSSL::X509::Name.parse(name)
      key = OpenSSL::PKey::RSA.new(2048)
      crt = OpenSSL::X509::Certificate.new

      crt.version = 2
      crt.serial  = Random.new.rand(100000)
      crt.subject = ca
      crt.issuer  = ca
      crt.public_key = key.public_key
      crt.not_before = Time.now
      crt.not_after  = Time.now + (1 * 365 * 24 * 3600) # 1yr

      ef  = OpenSSL::X509::ExtensionFactory.new
      ef.subject_certificate = crt
      ef.issuer_certificate  = crt
      crt.extensions = [
        ef.create_extension("basicConstraints", "CA:TRUE", true),
        ef.create_extension("subjectKeyIdentifier", "hash")
      ]
      crt.add_extension ef.create_extension("authorityKeyIdentifier",
                                            "keyid:always,issuer:always")
      crt.sign key, OpenSSL::Digest::SHA256.new

      # Store key

      # create key directory
      if not File.exist?("key")
        Dir.mkdir("key")
      end

      # server.key
      File.open(KEY_FILENAME, 'w+') do |f|
        f.write(key.to_pem)
      end

      # server.crt
      File.open(CRT_FILENAME, 'w+') do |f|
        f.write(crt.to_pem)
      end
    end

    # If the key does not exist, create the key
    def self.ensureKey
      if not File.exist?("key/server.crt")
        Occam::HTTPS.createKey
      end
    end
  end
end
