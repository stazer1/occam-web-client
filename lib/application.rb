# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

require_relative 'silence_warnings'

silence_warnings do
  require 'rbconfig'
  if (RbConfig::CONFIG['host_os'] =~ /mswin|mingw|cygwin/)
    require 'em/pure_ruby'
  end

  require 'bundler'
  Bundler::require

  require_relative '../config/environment'
  require_relative './websocket'
end

# Application root.
class Occam < Sinatra::Base
  # This module contains the many custom helpers for the Occam web client.
  module Helpers
  end

  # This module containers route handlers
  module Controllers
  end

  # Optionally use airbreak to log warnings in a sortable, quantifiable way.
  if Occam::Config.configuration['airbrake']['host']
    puts "Enabling Airbrake"

    require 'airbrake'

    Airbrake.configure do |config|
      # Alias
      occam_config = Occam::Config.configuration

      config.host        = occam_config['airbrake']['host']
      config.project_id  = occam_config['airbrake']['project-id']
      config.project_key = occam_config['airbrake']['project-key']
    end

    Airbrake.add_filter do |notice|
      # Retrieve the sinatra error class
      error = notice.stash[:rack_request].env["sinatra.error"]

      # Ignore 404s
      notice.ignore! if error.is_a? Sinatra::NotFound
    end

    use Airbrake::Rack::Middleware
  end

  # Use sessions

  # Determine the 'secure' attribute status of the session
  secureOn = Occam::Config.configuration['secure-session']

  # If the configuration does not specify specifically true/false, then we
  # default to true if and only if it is production mode.
  if secureOn.nil?
    secureOn = (ENV['RACK_ENV'] == "production")
  end

  enable :sessions
  set :session_secret, Occam::Config.configuration['secret']
  set :sessions, {
    :key => 'rack.session',
    :secret => Occam::Config.configuration['secret'],
    :expire_after => 2592000,
    :same_site => true,
    :secure => secureOn
  }

  # I18n
  I18n.load_path += Dir[File.join(File.dirname(__FILE__), "..", 'locales', '**', '*.yml')]
  I18n.load_path += Dir[File.join(File.dirname(__FILE__), "..", 'plugins', '*', 'locales', '**', '*.yml')]
  I18n.load_path += Dir[File.join(Gem::Specification.find_by_name('rails-i18n').gem_dir, 'rails', 'locale', '*.yml')]
  I18n.default_locale = :en

  # SASS
  require 'sass/plugin/rack'
  Sass::Plugin.options[:template_location] = "./stylesheets"
  Sass::Plugin.options[:css_location]      = "./public/stylesheets"

  configure :production do
    Sass::Plugin.options[:style] = :compressed
    Sass::Plugin.options[:cache] = true
    Sass::Plugin.update_stylesheets
    Sass::Plugin.options[:never_update] = true
  end

  use Sass::Plugin::Rack

  def self.load_tasks
    Dir[File.join(File.dirname(__FILE__), "tasks", '**', '*.rb')].each do |file|
      require file
    end
  end

  # Sets the domain
  def self.domain=(value)
    @@domain = value
  end

  # Returns the known domain for this running server
  def self.domain
    if !defined?(@@domain)
      @@domain = nil
    end
    @@domain
  end

  def self.occamPath
    if !defined?(@@occamPath)
      # The OCCAM server should know itself
      puts "Looking for path of occam toolchain, if it exists."
      occamPath = `which occam`.strip
      if occamPath
        occamPath = File.join(File.dirname(occamPath), "..")
      end
      if not File.exist?(occamPath)
        occamPath = File.join(File.dirname(__FILE__), "..", "..", "occam")
      end
      if not File.exist?(occamPath)
        occamPath = File.join(File.dirname(__FILE__), "..", "..", "occam-worker")
      end
      occamPath = File.realpath(occamPath)

      puts "Occam binaries found in: #{occamPath}"
      @@occamPath = occamPath
    end

    @@occamPath
  end

  # Load the daemon localization strings
  if Occam.occamPath
    puts "Loading Occam specific localization."
    I18n.load_path += Dir[File.join(Occam.occamPath, 'locales', '**', '*.yml')]
  end
end

# The base controller
require_relative './controller'

# Runs occam commands
require_relative './worker'

# This helps handle chunked streams and long-term content downloads
require_relative './stream'

# The occam daemon backend
require_relative './daemon'

# The OCCAM server needs an SSL key for replication
require_relative './https'
Occam::HTTPS.ensureKey

# We need to tell the daemon about ourselves
if (ENV["RACK_ENV"] || "development").intern != :test
  begin
    config = Occam::Config.configuration
    Occam::Worker.perform("discover", "announce-client", ["occam-web-client", config["port"]], {})
  rescue
    puts "*** Could not talk to the Occam daemon. ***"
    puts "***                                     ***"
    puts "*** Please start the Occam daemon or    ***"
    puts "*** configure the correct port in your  ***"
    puts "*** web-config.yml file in your Occam   ***"
    puts "*** home path (~/.occam by default)     ***"
    exit
  end

  if not File.exists?(File.join("public", "images", "symbol", "ui.svg"))
    puts "*** Could not find image icon assets.   ***"
    puts "***                                     ***"
    puts "*** Please generate the image assets by ***"
    puts "*** running the asset compiler.         ***"
    puts "***                                     ***"
    puts "*** npm run compile-assets              ***"
    exit
  end
end

# Load application source
%w(helpers models).each do |dir|
  Dir[File.join(File.dirname(__FILE__), "..", dir, '**', '*.rb')].each do |file|
    require_relative file
  end
end

class Occam
  module Controllers
    # The metadata controller
    class MetadataController < Occam::Controller
    end
  end
end

# Initialize metadata routes
Occam::Metadata.list.keys.each do |key|
  puts "[Occam::Metadata]: Initializing #{key} routes."

  metadata = Occam::Metadata.list[key]
  urls = metadata[:urls] || []
  routes = {}

  urls.each do |info|
    info[:key] = key
    routes[info[:root]] = info
  end

  controller = Class.new(Occam::Controllers::MetadataController) do
    def call(env)
      routes = self.class.class_variable_get(:@@routes)
      request = Rack::Request.new(env)
      info = nil
      # TODO: handle subpaths
      if routes.include?(request.path)
        info = routes[request.path]

        # Check "Accept" headers
        def parse_http_accept_header(header)
          header.to_s.split(/\s*,\s*/).map do |part|
            attribute, parameters = part.split(/\s*;\s*/, 2)
            quality = 1.0
            if parameters and /\Aq=([\d.]+)/ =~ parameters
              quality = $1.to_f
            end
            [attribute, quality]
          end
        end

        found = false
        parse_http_accept_header(request.get_header("HTTP_ACCEPT")).each do |item|
          if (info[:accept].include?(item[0]))
            found = true
          end
        end

        if not found
          request = nil
        end
      else
        request = nil
      end

      # Check to see if this matches the route and accept
      if request
        # Get the HTTP method
        method = request.request_method

        # Parse the query parameters or form data
        query = Rack::Utils.parse_nested_query(request.query_string)

        # Get the headers for this request
        headers = env.select{ |k,v| k.start_with? 'HTTP_'}.transform_keys do |k|
          k[5...].split('_').map(&:capitalize).join('-')
        end

        status, headers, response = Occam::Metadata.http(info[:key], method.upcase, "/", query, headers, nil)
      else
        # Otherwise continue with normal processing
        status, headers, response = @app.call(env)
      end

      [status, headers, response]
    end
  end

  controller.class_variable_set(:@@routes, routes)

  Occam.class_eval do
    use controller
  end
end

# Load application routes
%w(controllers plugins/*/controllers).each do |dir|
  Dir[File.join(File.dirname(__FILE__), "..", dir, '**', '*.rb')].each do |file|
    require_relative file
  end
end

class Occam
  module Controllers
    # The 400/500 controller
    class ErrorController < Occam::Controller
      # 404 route
      not_found do
        # Do not GZIP the 404 page
        cache_control "no-transform"

        render(:slim, :"static/404")
      end

      # 500 route.
      error do
        # Do not GZIP the 500 page
        cache_control "no-transform"

        render(:slim, :"static/500")
      end
    end
  end

  use Controllers::ErrorController

  def call(env)
    # Allows root not_found/error pages
    Occam::Controllers::ErrorController.call(env)
  end
end
