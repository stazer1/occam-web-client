# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This class represents a connection to an Occam daemon.
  class Daemon
    require 'socket'
    require 'json'

    attr_reader :host
    attr_reader :port

    attr_reader :socket

    class PromotedSocket
      attr_reader :socket

      def initialize(socket, daemon)
        @socket = socket
        @daemon = [daemon]

        ObjectSpace.define_finalizer(self, self.class.finalize(@daemon))
      end

      def send(*args, &block)
        if @socket
          @socket.send(*args, &block)
        end
      end

      def read(*args, &block)
        if @socket
          @socket.read(*args, &block)
        end
      end

      def readline(*args, &block)
        if @socket
          @socket.readline(*args, &block)
        end
      end

      def write(*args, &block)
        if @socket
          @socket.write(*args, &block)
        end
      end

      def close
        if @daemon[0]
          @socket = nil
          @daemon[0].disconnect
          @daemon[0] = nil

          # Promoted daemons must 'check in' to the pool again
          # so the connection is allowed to be re-established.
          Occam::Worker.daemons.checkin
        end
      end

      def close_read
        if @socket
          @socket.close_read
        end
      end

      def close_write
        if @socket
          @socket.close_write
        end
      end

      def self.finalize(daemon)
        proc {
          # Disconnect from the daemon when the promoted socket loses scope
          if daemon[0]
            daemon[0].disconnect
            daemon[0] = nil
          end
        }
      end
    end

    class Error < StandardError
      attr_reader :message
      attr_reader :response

      def initialize(message, response)
        @response = response
        @message  = message

        super(message)
      end
    end

    def initialize(host=nil, port=nil)
      # Creates an instance by connecting to the running daemon.

      # Get configuration
      @config = Occam::Config.configuration()

      # Gather the host and port for the daemon
      @host = host || (@config['daemon'] || {})['host'] || "localhost"
      @port = port || (@config['daemon'] || {})['port'] || 32000

      @promoted = false
      @socket = [nil]

      # Finalize to close the socket cleanly
      ObjectSpace.define_finalizer(self, self.class.finalize(@socket))
    end

    # Connects to the Occam daemon.
    def connect(retries = 5)
      # If the connection fails, retry until `retries` is reached.
      retries.times do
        begin
          @socket = [TCPSocket.new(@host, @port)]
          return @socket
        rescue Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::EPIPE, RuntimeError => e
          # If we fail, we loop to try again with a different connection
          sleep 0.3
        end
      end

      # At this point we know this is some sort of communication issue.
      ret = {
        :code => -1,
        :header => {
          :status => "error",
          :message => "Unable to connect to backend."
        },
        :data => ""
      }

      # Raise an Occam::Daemon::Error to emulate a daemon failure
      raise Occam::Daemon::Error.new(ret[:header][:message], ret)
    end

    # Returns a Hash representing the response for the command.
    def execute(component, command, arguments = [], options = {}, stdin = nil, promote = false, retries = 5)
      if @socket[0].nil?
        self.connect(retries)
      end

      if promote
        if @promoted
          raise "Cannot issue a command to a promoted daemon"
        end
        @promoted = true
      end

      begin
        options.each do |k,v|
          if v != true
            if v.is_a? Array
              options[k] = v.map do |item|
                if item.is_a? Array
                  item.dup.map(&:to_s)
                else
                  item.to_s
                end
              end
            else
              options[k] = v.to_s
            end
          end
        end

        payload = {}
        payload["component"] = component
        payload["command"] = command
        payload["arguments"] = arguments.map(&:to_s)
        payload["options"] = options

        if promote
          payload["promote"] = true
        end

        if stdin
          if not stdin.is_a? String
            raise "STDIN should be a string"
          end
          payload["stdin"] = stdin.length
        end
        payload = payload.to_json

        # Send the command
        @socket[0].puts payload

        if stdin
          @socket[0].write stdin
        end

        # Wait for response
        headerSize = @socket[0].gets
        # We expect a header in the response, always. Without one we hit a JSON
        # parsing error below.
        if headerSize.nil?
          raise "Connection reset while getting response header."
        end
        headerSize = headerSize.to_i

        # Read header
        headerData = @socket[0].read(headerSize)
        if headerSize != 0 and headerData.length != headerSize
          raise "Connection to daemon failed: short read"
        end
        headerData = JSON.parse(headerData, :symbolize_names => true)

        # Read Data size
        dataSize = @socket[0].gets
        if dataSize.nil?
          raise "Connection to daemon failed: failed to get message data size."
        end
        dataSize = dataSize.to_i

        # Read Data
        data = @socket[0].read(dataSize)

        if dataSize != 0 and data.length != dataSize
          raise "Connection to daemon failed: short read"
        end

        ret = {
          :code   => headerData[:code] || (headerData[:status] == "ok" ? 0 : -1),
          :header => headerData,
          :data   => data
        }
      rescue Errno::ECONNREFUSED, Errno::ECONNRESET, Errno::EPIPE, RuntimeError => e
        # Transient failure: we want the worker to recreate this connection.
        self.disconnect
        raise e
      rescue Exception => e
        ret = {
          :code => -1,
          :header => {
            :status => "error",
            :message => e.message
          },
          :data => ""
        }
      end

      if ret[:header][:status] == "error"
        raise Occam::Daemon::Error.new(ret[:header][:message], ret)
      end

      if promote
        # Keep an instance of the daemon so the socket isn't closed
        # until the socket is destroyed or drops out of scope
        PromotedSocket.new(@socket[0], self)
      else
        ret
      end
    rescue IOError
      # If the socket disconnects somehow prior to or during sending this
      # command, we need to reconnect. And then try it again.
      @socket[0] = nil
      if retries > 0
        self.execute(component, command, arguments, options, stdin, promote, retries - 1)
      else
        nil
      end
    end

    def disconnect
      if @socket[0]
        @socket[0].close
        @socket[0] = nil
      end
      @promoted = false
    end

    def self.finalize(socket)
      proc {
        if socket[0]
          socket[0].close
          socket[0] = nil
        end
      }
    end
  end
end
