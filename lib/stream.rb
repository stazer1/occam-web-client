# This file is in the public domain.
#
# This will ensure all streams are chunked when using the "chunked_stream" helper.


# wilkie:
# I need a separate chunked_stream since slim's streaming doesn't like this
# interaction alongside the GZIP package. I don't know why. When you do the
# normal "stream :keep_open do" in Sinatra along with nginx as a load balancer,
# the chunked streaming works. When you access the Puma server directly however,
# the browser reports ERR_INVALID_CHUNKED_ENCODING in Chrome. Baffling.
#
# So now I have wrapped the streaming helper with chunked_stream which forces
# the chunked transfer encoding. Works with and without nginx.

# This code is based off of: https://gist.github.com/uu59/1671771
module Sinatra
  class Controller
    module Helpers
      class ChunkedStream < Sinatra::Helpers::Stream
        def each(&front)
          @front = front
          callback do
            @front.call("0\r\n\r\n")
          end

          @scheduler.defer do
            begin
              @back.call(self)
            rescue Exception => e
              @scheduler.schedule { raise e }
            end
            close unless @keep_open
          end
        end

        def <<(data)
          @scheduler.schedule do
            size = data.to_s.bytesize
            if size != 0
              @front.call([size.to_s(16), "\r\n", data.to_s, "\r\n"].join)
            end
          end
          self
        end
      end
    end
  end
end

class Occam
  class Controller
    # This class wraps and io to output http chunked encoding chunks.
    #
    # Only useful when we need to use rack.hijack (which we don't atm)
    class ChunkWriter
      def initialize(io)
        @io = io
      end

      def callback(&block)
        @callback = block
      end

      def <<(data)
        puts "Hereeeeeeeeeeeeeeeeeeeeeeeeeeee          2"
        size = data.to_s.bytesize
        @io.write([size.to_s(16), "\r\n", data.to_s, "\r\n"].join)
      rescue
        if @callback
          yield(@callback)
        end
      end
    end

    # This will stream out content, much like Sinatra/Rack's common 'stream' function,
    # but ensure it uses a chunked transfer method.
    def chunked_stream(*arg)
      # This would be for any non-conforming servers
      # Those can be checked via 'settings.server == :puma' etc
      out = env['rack.hijack'].call
      out = env['rack.hijack_io']
      out.write "HTTP/1.1 #{response.status} OK\r\n"
      out.write "Status: #{response.status}\r\n"
      out.write "Connection: keep-alive\r\n"
      response.headers.each do |k,v|
        out.write "#{k}: #{v}\r\n"
      end
      out.write "Transfer-Encoding: chunked\r\n"
      out.write "X-Content-Type-Options: nosniff\r\n"
      out.write "\r\n"

      yield(ChunkWriter.new(out))

      out.write("0\r\n\r\n")
      out.close

      return [-1, {}, []]
    end
  end
end
