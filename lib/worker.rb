# OCCAM - Digital Computational Archive and Curation Service
# Copyright (C) 2017-2022 wilkie
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

class Occam
  # This class allows commands to be invoked on the Occam daemon.
  #
  # It will pull a daemon connection from a pool of possible existing
  # connections using a connection pool. It then uses that connection to
  # provide a response for the given command.
  #
  # A promoted command is one where the daemon socket is used bi-directionally
  # or over a longer period to stream data. This is useful for transferring
  # large file data, job logs, or for interacting with jobs in a terminal.
  #
  # Promoted commands yield a promoted socket which holds a daemon connection
  # from the pool hostage until the socket is closed. During this time, the
  # pool will not provide that daemon connection to the client.
  #
  # The size of the daemon connection pool is a carefully considered number.
  # The daemon itself can generally only support a certain number of database
  # connections. Each daemon connection *may*, while not guaranteed, use one
  # database connection. Long-term daemon activity, such as streaming a file,
  # will close that database connection before going into its streaming phase,
  # so it is not as much of an issue.
  #
  # Therefore, worst case is you flood the daemon with short requests which, in
  # turn, flood database connections. The web client needs to ensure that the
  # maximum daemon connections (the pool size) does not exceed the maximum
  # number of database connections. The pool size is *per process*, therefore
  # the pool size should be the number of database connections divided by the
  # number of web workers. Note, this is saturated only optimistically by the
  # scheduler of the web workers themselves. That is, four workers that
  # overworks one or two will never make use of the daemon connections. The
  # right number will require some investigation of analytics.
  module Worker
    def self.perform(component, command, arguments=[], options = {}, data=nil, promote = false, type="command", retries=5)
      # Attempt to execute the command on the backend.
      #
      # If the connection fails, retry until `retries` is reached.
      conn = nil

      # Our goal is to get a response from a daemon
      ret = nil

      # Retrieve a reference to the connection pool
      daemons = self.daemons

      if promote
        # Promoted daemons are 'checked out' of the pool and have to be
        # placed back in when the daemon disconnects. This effectively
        # reference counts the connection in this particular thread... so
        # promoted daemons need to take care that they are ONLY used within
        # just one thread and cleaned successfully (the socket is closed.)
        conn = daemons.checkout
        conn.execute(component, command, arguments, options, data, promote, retries)
      else
        # Find a connection within the daemon connection pool and execute the
        # command requested. Note that daemons.with automatically handles
        # checkin() of the socket used for the connection at the end of
        # execution.
        daemons.with do |conn|
          ret = conn.execute(component, command, arguments, options, data, promote, retries)
        end

        # If we have a response, return it
        ret
      end
    end

    def self.daemons
      # Create a daemon connection pool, if necessary, for this process
      if !defined?(@@pool)
        maxConnections = Occam::Config.configuration['daemon']['max-connections']
        timeout = Occam::Config.configuration['daemon']['timeout']

        # Create a new connection pool using the configuration provided
        # The timeout is the number of seconds to wait for a connection from
        # the pool. It will raise when it cannot.
        @@pool = ConnectionPool.new(:size => maxConnections, :timeout => timeout) do
          Occam::Daemon.new
        end
      end

      @@pool
    end
  end
end
