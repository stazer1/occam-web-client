class Occam
  # This module is the base class for a route handler.
  class Controller < Sinatra::Base
    # Establish the environment
    set :environment, (ENV["RACK_ENV"] || "development").intern

    # Use root directory as root
    set :app_file => '.'
    set :server, :puma

    # GZIP
    use Rack::Deflater

    # Chunk responses with unknown Content-Length
    use Rack::Chunked

    # Static Asset Management
    set :static_cache_control, [:public, max_age: 60 * 60 * 24 * 365]

    rootPath = File.realpath(File.join(File.dirname(__FILE__), ".."))
    set :slim, :views => File.join(rootPath, "views")

    # Markdown
    Tilt.register Tilt::RedcarpetTemplate, 'md'
    Tilt.prefer   Tilt::RedcarpetTemplate
    set :markdown, :layout_engine => :slim,
                   :layout        => :markdown,
                   :fenced_code_blocks => true

    # SSL
    set :ssl_certificate, File.join(File.dirname(__FILE__), "..", "key", "server.crt")
    set :ssl_key,         File.join(File.dirname(__FILE__), "..", "key", "server.key")

    use Rack::MethodOverride
    use Occam::WebSocketBackend

    class RequestLogger
      def initialize(app)
        @app = app
      end

      def call(env)
        request = Rack::Request.new(env)
        if not request.path.end_with?("runs/running")
          puts "-------------------------"
          puts request.path
        end
        status, headers, response = @app.call(env)
        if not request.path.end_with?("runs/running")
          puts "  -> #{status}"
        end
        [status, headers, response]
      end
    end

    # Helpers
    helpers Sinatra::ContentFor

    # Reloader
    configure :development do
      puts "***** In Development Mode *****"

      silence_warnings do
        require 'sinatra/reloader'
      end

      # Possibly import Pry (debugger)
      begin
        require 'pry'
        puts "Debugging via Pry enabled."
      rescue LoadError
        puts "Debugging via Pry disabled (Pry not installed)."
      end

      puts "Using Reloader"

      register Sinatra::Reloader

      %w(helpers models controllers plugins/*/controllers).each do |dir|
        Dir[File.join(File.dirname(__FILE__), "..", dir, '**', '*.rb')].each do |file|
          also_reload file
        end
      end

      puts "*******************************"
    end

    helpers do
      # @api controller-helpers
      # Presenter helpers
      def handleCORS(origin=nil)
        # Allow Same-Origin CORS Requests for AJAX components
        origin = request.params["Origin"] || request.env["HTTP_ORIGIN"] || origin || "null"
        # TODO: figure out what I mean by this haha
        domain = origin
        if origin == "*"
          domain = "*"
        end

        if ["null", domain].include? origin
          headers "Access-Control-Allow-Origin" => origin,
                  "Access-Control-Allow-Headers" => "x-requested-with, range, accept-ranges, accept, Accept-Ranges, Content-Type, Authorization, X-Occam-Token",
                  "Access-Control-Expose-Headers" => "X-Occam-Token, X-Content-Length, Accept-Ranges, Content-Encoding, Content-Length, Content-Range, Authorization, Content-Type, Allow, Cache-Control",
                  "Access-Control-Allow-Methods" => "GET,OPTIONS",
                  "Origin" => origin

        end

        if origin == "null"
          headers "Access-Control-Allow-Credentials" => "true"
          headers "Vary" => "Origin"
        end
      end

      def partial(page, options={}, &block)
        if page.to_s.include? "/"
          page = page.to_s.sub(/[\/]([^\/]+)$/, "/_\\1")
        else
          page = "_#{page}"
        end

        render(:slim, page.to_sym, options.merge!(:layout => false), &block).to_s
      end

      def markdown(path, options={})
        path = settings.slim[:views] + "/" + path.to_s + ".md"
        markdown = options[:markdown] || File.open(path).read
        options.update({:fenced_code_blocks => true})
        engine = ::Redcarpet::Markdown.new(::Redcarpet::Render::HTML, options)
        engine.render(markdown)
      rescue
        ""
      end

      def render(engine, template, options = {}, locals = {}, &block)
        # Ensures @_out_buf is known to avoid warnings
        @_out_buf ||= nil

        # Slim calls the option :streaming, but Sinatra has a #stream method
        # so we also support the :stream option.
        options[:streaming] ||= options.delete(:stream)

        if engine == :slim && Occam::Config.configuration["stream"] != false
          options[:streaming] = true
        end

        # We are not streaming. Call super implementation
        return super unless options[:streaming]

        # Engine specific stuff, currently only :slim
        case engine
        when :slim
          # We use the Temple generator without preamble and postamble
          # which is suitable for streaming.
          options[:generator] = Temple::Generator
        else
          raise "Streaming is not supported for #{engine}"
        end

        # There is an output buffer present. We are already streaming, continue!
        if @_out_buf
          # Check if we are really streaming...
          raise 'You are trying to stream from within a unstreamed layout' unless @_out_buf.is_a? Sinatra::Helpers::Stream
          return super
        end

        # Right now, turn off GZIP... until we manually add it below
        # (Or patch Rack::Deflater to handle streams without falling apart at the seams)
        cache_control "no-transform"

        # Create a new stream
        stream :keep_open do |out|
          @_out_buf = out

          if options[:layout] == false
            # No layout given, start rendering template
            super
          else
            # Layout given
            layout = options[:layout] == nil || options[:layout] == true ? :layout : options[:layout]

            # Invert layout and template rendering order
            super engine, layout, options.merge(layout: false), locals do
              super engine, template, options.merge(layout: false), locals, &block
            end
          end
        end
      end
    end

    # 404 route
    not_found do
      # Do not GZIP the 404 page
      cache_control "no-transform"

      if body.nil? || body.empty?
        render(:slim, :"static/404")
      end
    end

    # 500 route.
    error do
      # Do not GZIP the 500 page
      cache_control "no-transform"

      if body.nil? || body.empty?
        render(:slim, :"static/500")
      end
    end

    def call(env)
      # Allows accept to be easily overriden
      request = Rack::Request.new(env)
      if request.params["accept"]
        request.env["HTTP_ACCEPT"] = request.params["accept"]
      end

      # Retain host
      if Occam.domain.nil?
        Occam.domain = "#{request.scheme}://#{request.host_with_port}"
      end

      super(env)
    end
  end
end
