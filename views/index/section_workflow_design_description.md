When you create new software (or wrap an existing tool), you will provide a set of configuration options and metadata that describes what kind of inputs and outputs your software deals with.
With this, the workflow editor knows what possible connections can be made, and what data it should expect gets generated when the software runs.
The amount of metadata is fairly light.
Just a few small tags and a JSON configuration schema, which you can see an example of [here](/QmXLMiJPYCnuBcHoYzYLeLghzQbYBgVC5RN5fy8yKCuTwd/5drrrsDviiZvYK6iRjh9XShqwyXh3b/files/input_schema.json).
