en:
  objects:
    metadata:
      installation:
        header: "Installation"

        fields:
          action: "Action"    # The action to take ("link", "copy")
          source: "Source"    # The path to the Object's file
          to: "Destination"   # The path of the new file
          summary: "Summary"  # The summary for this file

        # The different file system actions
        actions:
          copy: "Copy"
          link: "Link"

        submit: "Add Installation"

        none: |
          No files will be installed.

        # When no summary is given
        noSummary: |
          N/A

        help: |
          Objects have a set of files that comprise that object, as seen in the Files tab.
          When an object is built, they have a set of files that are the result of that
          build. In each case, these sets of files are the ones that are accessible within
          a virtual machine when running the object.

          This section describes how those files are accessible and visible to that machine.
          When a virtual machine is created, the data is always accessible read-only within
          a particular mounted directory. However, applications may expect to be located at
          particular directories. This section allows one to place files in different
          directories when the virtual machine is launched.

          There are two types of access. One is a **link** where the file is not copied from
          its original location. Instead, it creates what is called a *symbolic link* which
          is a "shortcut" that acts like a normal file but allows the actual data to be in a
          different directory. The other type is a traditional **copy** which copies the data
          explicitly to the provided destination. This takes more time, but the copy can be
          modified, unlike the read-only *link* type.
