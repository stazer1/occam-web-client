en:
  objects:
    # Object history viewer
    history:
      header: "History"
      buildUnavailable: "Published build not available. Please, create a build for this version."

      activity:
        header: "Activity"
        create: "Publish Snapshot"
        none: "No History" # When there are no commits to list

        fields:
          author: "Author"

        new:
          header: "Publish"
          description: "Save a shareable snapshot of this object."

          pending: "Saving a new snapshot..."

          tabs:
            general: "General"

          fields:
            summary: "Summary"

          submit: "Publish"

      versions:
        header: "Versions"
        none: "No Version Tags" # When there are no tagged versions to list
        create: "Publish Version"

        fields:
          name: "Name"
          revision: "Revision"
          signedBy: "Signed By"
          signedOn: "Signed On"

        new:
          header: "New Version"
          description: "Creates a tag that represents this snapshot."
          pending: "Creating and signing new version..."

          tabs:
            general: "General"

          fields:
            name: "Name"

          submit: "Publish"

      help: |
        This page lists the published changes made to the object.
        
        When you are editing the object, it will be in a special view called a
        **stage**. In this mode, edits can be made but are not publicly visible
        until they are published when a snapshot is made.

        The **Activity** tab lists these snapshots and the changes made to the
        object.

        Whenever you wish to create a snapshot that can be used by the public,
        you use the **Publish** button. You can then supply a message that
        indicates what changes have been made.

        The **Versions** tab lists the named snapshots of the artifact. These
        are specific points in time given a meaningful and typically ordered
        name. That is, a version might be "1.0" for the first stable release
        of some software or a dataset. The next might then be "2.0".

        This tab lists the known named versions and the actors that named them.
        These versions are verified by the system as having been performed at
        the provided time by the given actor. This is done through a
        cryptographic key signing process.
