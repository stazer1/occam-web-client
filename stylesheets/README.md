<!-- This is also the styleguide homepage for KSS. -->

# Style Guide

Here, Occam web portal developers will find information about stylesheets and how markup is used through the site.

In the Occam web portal, stylesheets are generated with the [Sass](https://sass-lang.com/) preprocessor.
Conventions are maintained via the [stylelint](https://stylelint.io/) project which compares the `.scss` files to rules and patterns and reports violations.

The stylesheet is accessed as the URL `/stylesheets/app.css` which it then generates, if it needs to, by looking at `stylesheets/app.scss`,
which is the root stylesheet file for the project.

The `app.scss` file then imports each of the other sections of the stylesheets from the remaining `.scss` files throughout the subdirectories within the `stylesheets` directory.
Each file is prepended with an underscore (e.g. `stylesheets/objects/_details.scss`) which denotes it as a *partial*, much like the `slim` templates are partial documents of a larger HTML page.
To import a partial, you simply use the Sass `@import` directive and give it a path *without* the underscore and relative to the `stylesheets` directory:

```
@import "objects/details.scss";
```

## Site Philosophy

Since this project provides both artifact creation and exploration, there is an emphasis on both usability and discovery.
As the site as a whole is a portal to Occam, and this is a tool devoted to artifact evaluation particularly in the sciences,
there is also a strong need for communication and a strong desire to remove ambiguity in the interfaces.
The archive tooling also provides quite a lot of extensibility in that widgets can be added and used that may have their own
opinions on design. We must allow this as enabling this extensibility goes toward enabling broad preservation.
We will try to accomodate, as best as we can, and provide hints to such widgets.

Here is a set of design opinions the site upholds:

### Artifacts are Read More than Written

* Hide details that are not immediately important to understanding the artifact.

### Discovery is Important

* No "infinite" scrolling as not providing a good sense of length causes folks to anxiously scroll.
* To that end, discovery is better done with a set of basic search and facets with the option for more advanced filters.

## Class Naming

This project uses a loose variation of [SMACSS](https://smacss.com/) style naming.
It prefers relational selectors that are tagged liberally throughout the page.
This is distinct to [BEM](http://getbem.com/) style naming, which takes the approach the reusability and modularity require strict class names that remove specificity.

Basically, I'm taking the bet that BEM will suffer and become difficult to maintain when web components and shadow DOM methods of providing scope to stylesheets becomes the norm.
When that day comes, the existing styling can be moved into their web component scope without changing the layout or site templates.
Hopefully this pans out, and also hopefully CSS understands that its lack of relational specificity hurts it so much people ignore specificity by writing really ridiculous class names.

Either way, elements on the page are given a descriptive class name based on their behavior and intended usage.
For instance, `<ul>` tags may have a `things` class name, and then subsequent child `<li>` elements have a `thing` class name.
The styling for these would be the SCSS as follows:

```css
ul.things {
  li.thing {
    /* Styling */
  }
}
```

The styles are written with base properties first, followed by sets of sub-rules that have ascending specificity.
Groups of style rules can be made by using a comment.
Otherwise all rules are grouped together with no newlines separating them.
There is no use of redundant or trailing whitespace.
All units must be specified, except for when "0" is used, which should appear without a unit.
Content pseudo selectors should have two colons (`::`) when used, such as in `::after`.
Again, these are rules that are checked by the style linter.

```css
ul.things {
  /* Basic Rules */
  background: #888;

  /* Positioning */
  display: relative
  top: 0;
  right: 0;

  /* A normal list item */
  li.thing {
    /* Basic rules for this */
    color: black;
  }

  /* A specific list item */
  li.thing.specific {
    /* Overwritten rules */
    color: red;
  }
}
```

## Project Layout

The stylesheets are laid out similarly to the layout of `slim` HTML templates.
Exceptions exist when certain sections may appear on several different pages.
Other subdirectories will contain individual components, such as buttons and dropdowns.

The basic layout is as follows:

```
stylesheets                       - Contains all stylesheet files
  | app.scss                      - The main stylesheet root
  | _base.scss                    - Basic rules to basic elements
  | _card.scss                    - Rules for basic div elements
  | _device.scss                  - Defines responsive design widths
  | _font.scss                    - Defines fonts
  | _header.scss                  - Styles the main h1 header
  | _help.scss                    - The help sections and buttons
  | _modal.scss                   - Modal support
  | _objects.scss                 - The root partial for all object pages
  | _run-list.scss                - The run list sidebar panel
  | _social.scss                  - The root partial for social sidebar panel
  | _structure.scss               - Defines the layout/structure of the page
  | _tab-bar.scss                 - The interactive bar at the top of many pages
  | _tabs.scss                    - The main and secondary tab strip
  | _topbar.scss                  - The top navigation and logo bar
  components
    | _button.scss                - Basic buttons
    | _object-container.scss      - Object lists and tables
    | _object-list.scss           - Object listing
    | _selector.scss              - Select element styling
    | _slider-checkbox.scss       - Checkbox element styling
    | _spinner.scss               - Loading graphic
    | _tooltip.scss               - Tooltip styling
  objects
    | _access.scss                - The "Access" tab
    | _details.scss               - The "Details" tab
    | _files.scss                 - The "Files" tab
    | _history.scss               - The "History" tab
    details
      | _citation.scss            - The citation section
      | _description.scss         - The object description section
      | _gallery.scss             - The object image gallery
      | _run.scss                 - The run/build/etc metadata section
    files
      | _directory-panel.scss     - The directory listing sidebar panel
      | _tabs.scss                - The file tabs
  social
    | _comment.scss               - A social comment and threaded comments
```
